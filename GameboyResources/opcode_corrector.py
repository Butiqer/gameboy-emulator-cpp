
import json

data = None
correct_data = None

with open('opcodes.json', 'r') as f:
    read_string = f.read()
    data = json.loads(read_string)
    correct_data = json.loads(read_string)

for opcode, info in data['unprefixed'].items():
    iop = int(opcode, 16)
    if iop >= 0xD3:
        iop += 1
    if iop >= 0xDB:
        iop += 1
    if iop >= 0xDD:
        iop += 1
    if iop >= 0xE3:
        iop += 2
    if iop >= 0xEB:
        iop += 3
    if iop >= 0xF4:
        iop += 1
    if iop >= 0xFC:
        iop += 2

    correct_data['unprefixed'][hex(iop)] = info


with open('opcodes_correct.json', 'w+') as f:
    f.write(json.dumps(correct_data, indent=2))