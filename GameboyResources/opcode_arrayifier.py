
import json, copy

class Operation:
    def __init__(self, opcode):
        self.opcode = opcode

    def __str__(self):
        return self.opcode


data = None
with open('opcodes_correct.json', 'r') as opcodes_file:
    data = json.loads(opcodes_file.read())

types = {}
types['DAA']    = 0
types['SCF']    = 1
types['ADD']    = 2
types['CCF']    = 3
types['AND']    = 4
types['SUB']    = 5
types['RET']    = 6
types['JR']     = 7
types['DEC']    = 8
types['NOP']    = 9
types['RST']    = 10
types['EI']     = 11
types['DI']     = 12
types['RRCA']   = 13
types['CP']     = 14
types['CALL']   = 15
types['RLA']    = 16
types['POP']    = 17
types['PUSH']   = 18
types['LDH']    = 19
types['LD8']    = 20
types['STOP']   = 21
types['OR']     = 22
types['CPL']    = 23
types['INC']    = 24
types['SBC']    = 25
types['RLCA']   = 26
types['HALT']   = 27
types['RRA']    = 28
types['ADC']    = 29
types['PREFIX'] = 30
types['XOR']    = 31
types['JP']     = 32
types['RETI']   = 33
types['LD16']   = 34

ops = []
for opcode, info in data['unprefixed'].items():
    op = Operation(opcode)
    op.name  = info['mnemonic']
    op.bytes = info['bytes']
    op.ticks = info['cycles']
    
    op.operand1 = info.get('operand1')
    op.operand2 = info.get('operand2')
    
    if op.name == 'LD' and (len(op.operand1) == 1 or len(op.operand2) == 1):
        op.name = 'LD8'
    elif op.name == 'LD':
        op.name = 'LD16'

    op.type = types[op.name]
    #op.type  = types[info['mnemonic']]
    #types.add(info['mnemonic'])
    ops.append(op)


missing_ops = []
for i in range(255):
    try:
        next(x for x in ops if int(x.opcode, 16) == i)
    except:
        op = Operation(hex(i))
        op.bytes = 1
        op.name = 'NOP'
        op.type = types[op.name]
        #op.opcode = '0x0'
        ops.append(op)


sops = sorted(ops, key=lambda t: int(t.opcode, 16))

print("ops count: " + str(len(sops)))
#print([op.opcode for op in sops])



operands = []
operand_index = 0
for op in sops:
    line_format = "{}, {}// {} {}- operand 1 for {}"
    if hasattr(op, 'operand1') and not op.operand1 in operands:
        operands.append(op.operand1)
        operand_index = len(operands) - 1
    elif hasattr(op, 'operand1'):
        try:
            operand_index = operands.index(op.operand1)
        except:
            operand_index = 0
    else:
        op.operand1 = 0
    line = line_format.format(operand_index, ' ' * (2 - len(str(operand_index))), op.operand1, ' ' * (5 - len(str(op.operand1))), op.name)
    #print(line)

for op in sops:
    line_format = "{}, {}// {} {}- operand 2 for {}"
    if hasattr(op, 'operand2') and not op.operand2 in operands:
        operands.append(op.operand2)
        operand_index = len(operands) - 1
    elif hasattr(op, 'operand2'):
        try:
            operand_index = operands.index(op.operand2)
        except:
            operand_index = 0
    else:
        op.operand2 = 0
    line = line_format.format(operand_index, ' ' * (2 - len(str(operand_index))), op.operand2, ' ' * (5 - len(str(op.operand2))), op.name)
    #print(line)

print(operands)

flags = ['NZ', 'Z', 'C', 'NC']
operand_types = ['register_value', 'register_address', 'const_value', 'const_address', 'flag', 'zero', 'SP', 'cbprefix', 'reset', 'sp_r8']
operands_types = [0, 0, 1, 0, 3, 0, 0, 0, 5, 0, 1, 0, 2, 0, 4, 1, 0, 4, 0, 4, 6, 1, 1, 2, 8, 7, 8, 2, 8, 8, 3, 1, 8, 8, 0, 8, 8, 2, 9]

for op in sops:
    print('{}, {}// {}'.format(op.ticks[0], ' ' * (3 - len(str(int(op.ticks[0])))), op.name))
#   op1 = ''
#   op2 = ''
#   if hasattr(op, 'operand1'):
#       op1 = op.operand1
#   if hasattr(op, 'operand2'):
#       op2 = op.operand2
    #print('void opcode_{}(CPU* cpu, MMU* mmu, Cartridge* rom)'.format(op.opcode))
    #print('{')
    #print('    // {}, ({}, {})'.format(op.name, op1, op2))
    #print('}')
    #print('')
    #if op.name == 'NOP':
#    print('&opcode_0x0,')
#    else:
#       print('&opcode_{},'.format(op.opcode))

#print(operand_types)

"""i = 0
for ot in operands_types:
    line_format = "operand_type::{}, {}// {}"
    print(line_format.format(operand_types[ot], ' ' * (17 - len(operand_types[ot])), operands[i]))
    i += 1"""

#[None, 'BC', '(BC)', 'B', '(a16)', 'HL', 'A', 'C', '0', 'DE', '(DE)', 'D', 'r8', 'E', 'NZ', '(HL+)', 'H', 'Z', 'L', 'NC', 'SP', '(HL-)', '(HL)', 'a16', '00H', 'CB', '08H', 'd8', '10H', '18H', '(a8)', '(C)', '20H', '28H', 'AF', '30H', '38H', 'd16', 'SP+r8']



"""for op in sops:
    line_format = "instruction_bytes[{}]{}= {};  //{}"
    line = line_format.format(int(op.opcode, 16), ' ' * (4 - len(str(int(op.opcode, 16)))), op.bytes, op.name)
    print(line)"""

"""for op in sops:
    line_format = "instructions::{},"
    line = line_format.format(op.name)
    print(line)"""

"""for op in sops:
    line_format = "{},{}// {}"
    line = line_format.format(op.bytes, ' ' * (4 - len(str(op.bytes))), op.name)
    print(line)"""

"""types_ordered = list(types.items())
print(types_ordered)
types_ordered = sorted(types_ordered, key=lambda t: t[1])

for type, v in types_ordered:
    print(type + ',')"""




























