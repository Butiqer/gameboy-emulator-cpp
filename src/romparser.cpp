//
//  romparser.cpp
//  GameBoy
//
//  Created by Robin Carlsson on 2016-05-27.
//  Copyright © 2016 Robin Carlsson. All rights reserved.
//

#include <iostream>
#include <fstream>

#include "romparser.hpp"

Cartridge::Cartridge(const char* filename)
{
    bool success = false;
    std::ifstream file(filename, std::ios::binary);
	
    if(file.is_open() && file.good())
    {
		file.seekg(0, file.end);
        this->size = file.tellg();
		this->data = new uint8_t[this->size];
        file.seekg(0, file.beg);
        
		file.read((char*)data, this->size);
        
		if(file)
		{
            success = true;
        }
		else 
		{
			auto readcount = file.gcount();
			success = false;
		}
        
        file.close();
    }

    this->loaded_successfully = success;
}