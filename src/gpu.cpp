//
//  gpu.cpp
//  GameBoy
//
//  Created by Robin Carlsson on 2016-08-10.
//  Copyright © 2016 Robin Carlsson. All rights reserved.
//

#include <iostream>

#include "gpu.hpp"
#include "mmu.hpp"
#include "romparser.hpp"
#include "cpu.hpp"

void gpu_init(GPU* gpu)
{
    gpu->ticks = 0;
    gpu->last_cpu_ticks = 0;
    gpu->mode = gpu_mode::GPU_MODE_HBLANK;
}

void gpu_step(GPU* gpu, CPU* cpu, MMU* mmu, Cartridge* rom)
{
    cpu->interrupt.flags = mmu_read_byte(mmu, rom, 0xFF0F);
    cpu->interrupt.enabled = mmu_read_byte(mmu, rom, 0xFFFF);
    
    gpu->control = mmu_read_byte(mmu, rom, 0xFF40);
    gpu->scrollx = mmu_read_byte(mmu, rom, 0xFF42);
    gpu->scrolly = mmu_read_byte(mmu, rom, 0xFF43);
    gpu->scanline = mmu_read_byte(mmu, rom, 0xFF44);
    
    gpu->ticks += cpu->ticks - gpu->last_cpu_ticks;
    gpu->last_cpu_ticks = cpu->ticks;
    
    switch (gpu->mode) {
        case GPU_MODE_HBLANK:
            if(gpu->ticks >= 204)
            {
                gpu->scanline++;
                
                if(gpu->scanline == 143)
                {
                    if(cpu->interrupt.enabled & CPU_INTERRUPT_VBLANK)
                    {
                        cpu->interrupt.flags |= CPU_INTERRUPT_VBLANK;
                    }
                    gpu->mode = gpu_mode::GPU_MODE_VBLANK;
                }
                else
                {
                    gpu->mode = gpu_mode::GPU_MODE_OAM;
                }
                gpu->ticks -= 204;
            }
            break;
        case GPU_MODE_OAM:
            if(gpu->ticks >= 80)
            {
                gpu->mode = gpu_mode::GPU_MODE_VRAM;
                
                gpu->ticks -= 80;
            }
            break;
        case GPU_MODE_VBLANK:
            if(gpu->ticks >= 456)
            {
                gpu->scanline++;
                
                if(gpu->scanline > 153) {
                    gpu->scanline = 0;
                    gpu->mode = gpu_mode::GPU_MODE_OAM;
                }
                
                gpu->ticks -= 456;
            }
            break;
        case GPU_MODE_VRAM:
            if(gpu->ticks >= 172)
            {
                gpu->mode = gpu_mode::GPU_MODE_HBLANK;
                gpu->ticks -= 172;
            }
            break;
    }
    
    mmu_write_byte(mmu, rom, 0xFF40, gpu->control);
    mmu_write_byte(mmu, rom, 0xFF42, gpu->scrollx);
    mmu_write_byte(mmu, rom, 0xFF43, gpu->scrolly);
    mmu_write_byte(mmu, rom, 0xFF44, gpu->scanline);
    
    mmu_write_byte(mmu, rom, 0xFF0F, cpu->interrupt.flags);
    mmu_write_byte(mmu, rom, 0xFFFF, cpu->interrupt.enabled);
}

void gpu_render_graphics(GPU* gpu, CPU* cpu, MMU* mmu, Cartridge* rom)
{
    uint16_t vram_start = 0x8000;
    uint16_t vram_end   = 0x97FF;
    
    for(uint16_t i = vram_start; i < vram_end; i += 2)
    {
        uint8_t byte1 = mmu_read_byte(mmu, rom, i);
        uint8_t byte2 = mmu_read_byte(mmu, rom, i + 1);
        for(int j = 0; j < 8; j++)
        {
            uint8_t bit1 = (byte1 >> (7 - j)) & 1;
            uint8_t bit2 = (byte2 >> (7 - j)) & 1;
            std::cout << ((bit1 << 1) | bit2);
        }
        
        std::cout << std::endl;
    }
}
















