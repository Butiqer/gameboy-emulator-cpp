//
//  audio.cpp
//  GameBoy
//
//  Created by Robin Carlsson on 2016-12-27.
//  Copyright © 2016 Robin Carlsson. All rights reserved.
//

#include <cmath>

#include "audio.hpp"

#define AUDIO_FREQUENCY 44100
#define AUDIO_AMPLITUDE 8000

static SDL_AudioSpec audio_spec;
//static int frequency = 600;
//static double v = 0;
static Channel channel1;
static Channel channel2;
static Channel channel3;

void my_audio_callback(void *userdata, Uint8 *_stream, int _len)
{
    Sint16* stream = (Sint16*)_stream;
    int len = _len / 2;
    
    for(int i = 0; i < len; i++)
    {
        channel1.buffer[i] = AUDIO_AMPLITUDE / 2 * (std::sin(channel1.wave * 2 * M_PI / AUDIO_FREQUENCY) > 0 ? 1 : 0);
        channel1.wave += channel1.frequency;
        
        channel2.buffer[i] = AUDIO_AMPLITUDE / 2 * (std::sin(channel2.wave * 2 * M_PI / AUDIO_FREQUENCY) > 0 ? 1 : 0);
        channel2.wave += channel2.frequency;
        
        channel3.buffer[i] = AUDIO_AMPLITUDE * std::sin(channel3.wave * 2 * M_PI / AUDIO_FREQUENCY);
        channel3.wave += channel3.frequency;
    }
    SDL_memset(_stream, 0, _len);
    SDL_MixAudioFormat((Uint8*)stream, (Uint8*)channel1.buffer, AUDIO_S16SYS, len * 2, SDL_MIX_MAXVOLUME);
    SDL_MixAudioFormat((Uint8*)stream, (Uint8*)channel2.buffer, AUDIO_S16SYS, len * 2, SDL_MIX_MAXVOLUME);
    SDL_MixAudioFormat((Uint8*)stream, (Uint8*)channel3.buffer, AUDIO_S16SYS, len * 2, SDL_MIX_MAXVOLUME);
}

void audio_init()
{
    audio_spec.freq = AUDIO_FREQUENCY;
    audio_spec.callback = my_audio_callback;
    audio_spec.userdata = nullptr;
    audio_spec.format = AUDIO_S16SYS;
    audio_spec.channels = 1;
    audio_spec.samples = 2048;
    SDL_OpenAudio(&audio_spec, nullptr);
    SDL_PauseAudio(0);
    
    channel1.buffer = new Sint16[2048];
    channel1.frequency = 0;
    channel1.wave = 0;
    channel2.buffer = new Sint16[2048];
    channel2.frequency = 0;
    channel2.wave = 0;
    channel3.buffer = new Sint16[2048];
    channel3.frequency = 0;
    channel3.wave = 0;
}

void audio_set_frequency(int freq)
{
    //frequency = freq;
    channel1.frequency = freq;
}

void audio_set_frequency_c2(int freq)
{
    //frequency = freq;
    channel2.frequency = freq;
}

void audio_set_frequency_c3(int freq)
{
    //frequency = freq;
    channel3.frequency = freq;
}










