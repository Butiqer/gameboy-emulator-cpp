//
//  opcodes.cpp
//  GameBoy
//
//  Created by Robin Carlsson on 2016-07-05.
//  Copyright © 2016 Robin Carlsson. All rights reserved.
//

#include <iostream>

#include "opcodes.h"
#include "cpu.hpp"
#include "mmu.hpp"

void opcode_0x0(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // NOP, (None, None)
}

void opcode_0x1(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD16, (BC, d16)
    cpu->BC = get_operand_16(cpu, mmu, rom);
}

void opcode_0x2(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, ((BC), A)
    mmu_write_byte(mmu, rom, cpu->BC, *cpu->A);
}

void opcode_0x3(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // INC, (BC, None)
    cpu->BC = cpu_inc16(cpu, cpu->BC);
}

void opcode_0x4(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // INC, (B, None)
    (*cpu->B) = cpu_inc8(cpu, *cpu->B);
}

void opcode_0x5(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // DEC, (B, None)
    (*cpu->B) = cpu_dec8(cpu, *cpu->B);
}

void opcode_0x6(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (B, d8)
    (*cpu->B) = get_first_operand_8(cpu, mmu, rom);
}

void opcode_0x7(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // RLCA, (None, None)
    bool carry = carry_rotl8(*cpu->A);
    (*cpu->A) = rotl8(*cpu->A, 1);
    cpu_update_flags(cpu, *cpu->A == 0, false, false, carry);
}

void opcode_0x8(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD16, ((a16), SP)
    //uint16_t op1 = mmu_read_word(mmu, rom, cpu->PC + 1);
    uint16_t op1 = get_operand_16(cpu, mmu, rom);
    mmu_write_word(mmu, rom, op1, cpu->SP);
}

void opcode_0x9(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // ADD, (HL, BC)
    cpu->HL = cpu_add16(cpu, cpu->HL, cpu->BC);
}

void opcode_0xa(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (A, (BC))
    uint8_t op1 = mmu_read_byte(mmu, rom, cpu->BC);
    (*cpu->A) = op1;
}

void opcode_0xb(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // DEC, (BC, None)
    cpu->BC = cpu_dec16(cpu, cpu->BC);
}

void opcode_0xc(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // INC, (C, None)
    (*cpu->C) = cpu_inc8(cpu, *cpu->C);
}

void opcode_0xd(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // DEC, (C, None)
    (*cpu->C) = cpu_dec8(cpu, *cpu->C);
}

void opcode_0xe(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (C, d8)
    (*cpu->C) = get_first_operand_8(cpu, mmu, rom);
}

void opcode_0xf(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // RRCA, (None, None)
    bool carry = carry_rotr8(*cpu->A);
    (*cpu->A) = rotr8(*cpu->A, 1);
    
    cpu->FZ = (*cpu->A) == 0;
    cpu->FN = false;
    cpu->FH = false;
    cpu->FC = carry;
}

void opcode_0x10(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // STOP, (0, None)
    std::cout << "STOP not implemented" << std::endl;
    //cpu->PC = 0x0;
}

void opcode_0x11(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD16, (DE, d16)
    cpu->DE = get_operand_16(cpu, mmu, rom);
}

void opcode_0x12(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, ((DE), A)
    mmu_write_byte(mmu, rom, cpu->DE, *cpu->A);
}

void opcode_0x13(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // INC, (DE, None)
    cpu->DE = cpu_inc16(cpu, cpu->DE);
}

void opcode_0x14(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // INC, (D, None)
    (*cpu->D) = cpu_inc8(cpu, *cpu->D);
}

void opcode_0x15(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // DEC, (D, None)
    (*cpu->D) = cpu_dec8(cpu, *cpu->D);
}

void opcode_0x16(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (D, d8)
    (*cpu->D) = get_first_operand_8(cpu, mmu, rom);
}

void opcode_0x17(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // RLA, (None, None)
    bool carry = carry_rotl8(*cpu->A);
    (*cpu->A) = ((*cpu->A) << 1) + uint8_t(cpu->FC);
    
    cpu->FZ = (*cpu->A) == 0;
    cpu->FN = false;
    cpu->FH = false;
    cpu->FC = carry;
}

void opcode_0x18(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // JR, (r8, None)
    uint8_t op1 = get_first_operand_8(cpu, mmu, rom);
    cpu->PC += (int8_t)op1;// - instruction_bytes[0x18];
}

void opcode_0x19(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // ADD, (HL, DE)
    cpu->HL = cpu_add16(cpu, cpu->HL, cpu->DE);
}

void opcode_0x1a(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (A, (DE))
    (*cpu->A) = mmu_read_byte(mmu, rom, cpu->DE);
}

void opcode_0x1b(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // DEC, (DE, None)
    cpu->DE = cpu_dec16(cpu, cpu->DE);
}

void opcode_0x1c(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // INC, (E, None)
    (*cpu->E) = cpu_inc8(cpu, *cpu->E);
}

void opcode_0x1d(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // DEC, (E, None)
    (*cpu->E) = cpu_dec8(cpu, *cpu->E);
}

void opcode_0x1e(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (E, d8)
    uint8_t op1 = get_first_operand_8(cpu, mmu, rom);
    (*cpu->E) = op1;
}

void opcode_0x1f(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // RRA, (None, None)
    bool carry = carry_rotr8(*cpu->A);
    (*cpu->A) = ((*cpu->A) >> 1) + uint8_t(cpu->FC);
    
    cpu->FZ = (*cpu->A) == 0;
    cpu->FN = false;
    cpu->FH = false;
    cpu->FC = carry;
}

void opcode_0x20(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // JR, (NZ, r8)
    if(!cpu->FZ)
    {
        cpu->PC += ((int8_t)get_first_operand_8(cpu, mmu, rom));// - instruction_bytes[0x20];
        cpu->ticks += 12;
    }
    else
    {
        cpu->ticks += 8;
    }
    
}

void opcode_0x21(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD16, (HL, d16)
    uint16_t op = get_operand_16(cpu, mmu, rom);
    cpu->HL = op;
}

void opcode_0x22(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, ((HL+), A)
    mmu_write_byte(mmu, rom, cpu->HL++, *cpu->A);
}

void opcode_0x23(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // INC, (HL, None)
    cpu->HL = cpu_inc16(cpu, cpu->HL);
}

void opcode_0x24(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // INC, (H, None)
    (*cpu->H) = cpu_inc8(cpu, *cpu->H);
}

void opcode_0x25(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // DEC, (H, None)
    (*cpu->H) = cpu_dec8(cpu, *cpu->H);
}

void opcode_0x26(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (H, d8)
    uint8_t op1 = get_first_operand_8(cpu, mmu, rom);
    (*cpu->H) = op1;
}

void opcode_0x27(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // DAA, (None, None)
    std::cout << "0x27: DAA not implemented!" << std::endl;
}

void opcode_0x28(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // JR, (Z, r8)
    if(cpu->FZ)
    {
        cpu->PC += (int8_t)get_first_operand_8(cpu, mmu, rom);// - instruction_bytes[0x28];
        cpu->ticks += 12;
    }
    else
    {
        cpu->ticks += 8;
    }
}

void opcode_0x29(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // ADD, (HL, HL)
    cpu->HL = cpu_add16(cpu, cpu->HL, cpu->HL);
}

void opcode_0x2a(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (A, (HL+))
    (*cpu->A) = mmu_read_byte(mmu, rom, cpu->HL++);
}

void opcode_0x2b(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // DEC, (HL, None)
    cpu->HL = cpu_dec16(cpu, cpu->HL);
}

void opcode_0x2c(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // INC, (L, None)
    (*cpu->L) = cpu_inc8(cpu, *cpu->L);
}

void opcode_0x2d(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // DEC, (L, None)
    (*cpu->L) = cpu_dec8(cpu, *cpu->L);
}

void opcode_0x2e(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (L, d8)
    uint8_t op1 = get_first_operand_8(cpu, mmu, rom);
    (*cpu->L) = op1;
}

void opcode_0x2f(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // CPL, (None, None)
    (*cpu->A) = ~(*cpu->A);
}

void opcode_0x30(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // JR, (NC, r8)
    if(!cpu->FC)
    {
        cpu->PC += (int8_t)get_first_operand_8(cpu, mmu, rom); // - instruction_bytes[30];
        cpu->ticks += 12;
    }
    else
    {
        cpu->ticks += 8;
    }
    
}

void opcode_0x31(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD16, (SP, d16)
    uint16_t op = get_operand_16(cpu, mmu, rom);
    cpu->SP = op;
}

void opcode_0x32(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, ((HL-), A)
    mmu_write_byte(mmu, rom, cpu->HL--, *cpu->A);
}

void opcode_0x33(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // INC, (SP, None)
    cpu->SP = cpu_inc16(cpu, cpu->SP);
}

void opcode_0x34(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // INC, ((HL), None)
    uint8_t v = mmu_read_byte(mmu, rom, cpu->HL);
    v = cpu_inc8(cpu, v);
    mmu_write_byte(mmu, rom, cpu->HL, v);
}

void opcode_0x35(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // DEC, ((HL), None)
    uint8_t v = mmu_read_byte(mmu, rom, cpu->HL);
    v = cpu_dec8(cpu, v);
    mmu_write_byte(mmu, rom, cpu->HL, v);
}

void opcode_0x36(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD16, ((HL), d8)
    uint8_t op = get_first_operand_8(cpu, mmu, rom);
    mmu_write_byte(mmu, rom, cpu->HL, op);
}

void opcode_0x37(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // SCF, (None, None)
    cpu->FC = true;
}

void opcode_0x38(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // JR, (C, r8)
    if(cpu->FC)
    {
        cpu->PC += (int8_t)get_first_operand_8(cpu, mmu, rom); // - instruction_bytes[38];
        cpu->ticks += 12;
    }
    else
    {
        cpu->ticks += 8;
    }
}

void opcode_0x39(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // ADD, (HL, SP)
    cpu->HL = cpu_add16(cpu, cpu->HL, cpu->SP);
}

void opcode_0x3a(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (A, (HL-))
    (*cpu->A) = mmu_read_byte(mmu, rom, cpu->HL--);
}

void opcode_0x3b(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // DEC, (SP, None)
    cpu->SP = cpu_dec16(cpu, cpu->SP);
}

void opcode_0x3c(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // INC, (A, None)
    (*cpu->A) = cpu_inc8(cpu, *cpu->A);
}

void opcode_0x3d(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // DEC, (A, None)
    (*cpu->A) = cpu_dec8(cpu, *cpu->A);
}

void opcode_0x3e(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (A, d8)
    uint8_t op = get_first_operand_8(cpu, mmu, rom);
    (*cpu->A) = op;
}

void opcode_0x3f(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // CCF, (None, None)
    cpu->FC = false;
}

void opcode_0x40(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (B, B)
    (*cpu->B) = *cpu->B;
}

void opcode_0x41(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (B, C)
    (*cpu->B) = *cpu->C;
}

void opcode_0x42(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (B, D)
    (*cpu->B) = *cpu->D;
}

void opcode_0x43(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (B, E)
    (*cpu->B) = *cpu->E;
}

void opcode_0x44(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (B, H)
    (*cpu->B) = *cpu->H;
}

void opcode_0x45(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (B, L)
    (*cpu->B) = *cpu->L;
}

void opcode_0x46(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (B, (HL))
    (*cpu->B) = mmu_read_byte(mmu, rom, cpu->HL);
}

void opcode_0x47(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (B, A)
    (*cpu->B) = *cpu->A;
}

void opcode_0x48(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (C, B)
    (*cpu->C) = *cpu->B;
}

void opcode_0x49(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (C, C)
    (*cpu->C) = *cpu->C;
}

void opcode_0x4a(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (C, D)
    (*cpu->C) = *cpu->D;
}

void opcode_0x4b(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (C, E)
    (*cpu->C) = *cpu->E;
}

void opcode_0x4c(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (C, H)
    (*cpu->C) = *cpu->H;
}

void opcode_0x4d(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (C, L)
    (*cpu->C) = *cpu->L;
}

void opcode_0x4e(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (C, (HL))
    (*cpu->C) = mmu_read_byte(mmu, rom, cpu->HL);
}

void opcode_0x4f(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (C, A)
    (*cpu->C) = *cpu->A;
}

void opcode_0x50(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (D, B)
    (*cpu->D) = *cpu->B;
}

void opcode_0x51(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (D, C)
    (*cpu->D) = *cpu->C;
}

void opcode_0x52(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (D, D)
    (*cpu->D) = *cpu->D;
}

void opcode_0x53(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (D, E)
    (*cpu->D) = *cpu->E;
}

void opcode_0x54(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (D, H)
    (*cpu->D) = *cpu->H;
}

void opcode_0x55(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (D, L)
    (*cpu->D) = *cpu->L;
}

void opcode_0x56(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (D, (HL))
    (*cpu->D) = mmu_read_byte(mmu, rom, cpu->HL);
}

void opcode_0x57(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (D, A)
    (*cpu->D) = *cpu->A;
}

void opcode_0x58(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (E, B)
    (*cpu->E) = *cpu->B;
}

void opcode_0x59(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (E, C)
    (*cpu->E) = *cpu->C;
}

void opcode_0x5a(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (E, D)
    (*cpu->E) = *cpu->D;
}

void opcode_0x5b(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (E, E)
    (*cpu->E) = *cpu->E;
}

void opcode_0x5c(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (E, H)
    (*cpu->E) = *cpu->H;
}

void opcode_0x5d(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (E, L)
    (*cpu->E) = *cpu->L;
}

void opcode_0x5e(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (E, (HL))
    (*cpu->E) = mmu_read_byte(mmu, rom, cpu->HL);
}

void opcode_0x5f(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (E, A)
    (*cpu->E) = *cpu->A;
}

void opcode_0x60(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (H, B)
    (*cpu->H) = *cpu->B;
}

void opcode_0x61(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (H, C)
    (*cpu->H) = *cpu->C;
}

void opcode_0x62(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (H, D)
    (*cpu->H) = *cpu->D;
}

void opcode_0x63(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (H, E)
    (*cpu->H) = *cpu->E;
}

void opcode_0x64(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (H, H)
    (*cpu->H) = *cpu->H;
}

void opcode_0x65(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (H, L)
    (*cpu->H) = *cpu->L;
}

void opcode_0x66(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (H, (HL))
    (*cpu->H) = mmu_read_byte(mmu, rom, cpu->HL);
}

void opcode_0x67(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (H, A)
    (*cpu->H) = *cpu->A;
}

void opcode_0x68(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (L, B)
    (*cpu->L) = *cpu->B;
}

void opcode_0x69(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (L, C)
    (*cpu->L) = *cpu->C;
}

void opcode_0x6a(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (L, D)
    (*cpu->L) = *cpu->D;
}

void opcode_0x6b(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (L, E)
    (*cpu->L) = *cpu->E;
}

void opcode_0x6c(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (L, H)
    (*cpu->L) = *cpu->H;
}

void opcode_0x6d(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (L, L)
    (*cpu->L) = *cpu->L;
}

void opcode_0x6e(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (L, (HL))
    (*cpu->L) = mmu_read_byte(mmu, rom, cpu->HL);
}

void opcode_0x6f(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (L, A)
    (*cpu->L) = *cpu->A;
}

void opcode_0x70(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, ((HL), B)
    mmu_write_byte(mmu, rom, cpu->HL, *cpu->B);
}

void opcode_0x71(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, ((HL), C)
    mmu_write_byte(mmu, rom, cpu->HL, *cpu->C);
}

void opcode_0x72(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, ((HL), D)
    mmu_write_byte(mmu, rom, cpu->HL, *cpu->D);
}

void opcode_0x73(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, ((HL), E)
    mmu_write_byte(mmu, rom, cpu->HL, *cpu->E);
}

void opcode_0x74(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, ((HL), H)
    mmu_write_byte(mmu, rom, cpu->HL, *cpu->H);
}

void opcode_0x75(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, ((HL), L)
    mmu_write_byte(mmu, rom, cpu->HL, *cpu->L);
}

void opcode_0x76(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // HALT, (None, None)
    if(cpu->interrupt.master_enabled)
        cpu->PC -= instruction_bytes[0x76];
    //std::cout << "0x76: HALT not implemented!" << std::endl;
}

void opcode_0x77(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, ((HL), A)
    mmu_write_byte(mmu, rom, cpu->HL, *cpu->A);
}

void opcode_0x78(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (A, B)
    (*cpu->A) = *cpu->B;
}

void opcode_0x79(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (A, C)
    (*cpu->A) = *cpu->C;
}

void opcode_0x7a(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (A, D)
    (*cpu->A) = *cpu->D;
}

void opcode_0x7b(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (A, E)
    (*cpu->A) = *cpu->E;
}

void opcode_0x7c(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (A, H)
    (*cpu->A) = *cpu->H;
}

void opcode_0x7d(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (A, L)
    (*cpu->A) = *cpu->L;
}

void opcode_0x7e(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (A, (HL))
    (*cpu->A) = mmu_read_byte(mmu, rom, cpu->HL);
}

void opcode_0x7f(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (A, A)
    (*cpu->A) = *cpu->A;
}

void opcode_0x80(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // ADD, (A, B)
    (*cpu->A) = cpu_add8(cpu, *cpu->A, *cpu->B);
}

void opcode_0x81(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // ADD, (A, C)
    (*cpu->A) = cpu_add8(cpu, *cpu->A, *cpu->C);
}

void opcode_0x82(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // ADD, (A, D)
    (*cpu->A) = cpu_add8(cpu, *cpu->A, *cpu->D);
}

void opcode_0x83(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // ADD, (A, E)
    (*cpu->A) = cpu_add8(cpu, *cpu->A, *cpu->E);
}

void opcode_0x84(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // ADD, (A, H)
    (*cpu->A) = cpu_add8(cpu, *cpu->A, *cpu->H);
}

void opcode_0x85(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // ADD, (A, L)
    (*cpu->A) = cpu_add8(cpu, *cpu->A, *cpu->L);
}

void opcode_0x86(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // ADD, (A, (HL))
    (*cpu->A) = cpu_add8(cpu, *cpu->A, mmu_read_byte(mmu, rom, cpu->HL));
}

void opcode_0x87(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // ADD, (A, A)
    (*cpu->A) = cpu_add8(cpu, *cpu->A, *cpu->A);
}

void opcode_0x88(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // ADC, (A, B)
    (*cpu->A) = cpu_addc8(cpu, *cpu->A, *cpu->B);
}

void opcode_0x89(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // ADC, (A, C)
    (*cpu->A) = cpu_addc8(cpu, *cpu->A, *cpu->C);
}

void opcode_0x8a(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // ADC, (A, D)
    (*cpu->A) = cpu_addc8(cpu, *cpu->A, *cpu->D);
}

void opcode_0x8b(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // ADC, (A, E)
    (*cpu->A) = cpu_addc8(cpu, *cpu->A, *cpu->E);
}

void opcode_0x8c(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // ADC, (A, H)
    (*cpu->A) = cpu_addc8(cpu, *cpu->A, *cpu->H);
}

void opcode_0x8d(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // ADC, (A, L)
    (*cpu->A) = cpu_addc8(cpu, *cpu->A, *cpu->L);
}

void opcode_0x8e(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // ADC, (A, (HL))
    (*cpu->A) = cpu_addc8(cpu, *cpu->A, mmu_read_byte(mmu, rom, cpu->HL));
}

void opcode_0x8f(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // ADC, (A, A)
    (*cpu->A) = cpu_addc8(cpu, *cpu->A, *cpu->A);
}

void opcode_0x90(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // SUB, (B, None)
    (*cpu->A) = cpu_sub8(cpu, *cpu->A, *cpu->B);
}

void opcode_0x91(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // SUB, (C, None)
    (*cpu->A) = cpu_sub8(cpu, *cpu->A, *cpu->C);
}

void opcode_0x92(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // SUB, (D, None)
    (*cpu->A) = cpu_sub8(cpu, *cpu->A, *cpu->D);
}

void opcode_0x93(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // SUB, (E, None)
    (*cpu->A) = cpu_sub8(cpu, *cpu->A, *cpu->E);
}

void opcode_0x94(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // SUB, (H, None)
    (*cpu->A) = cpu_sub8(cpu, *cpu->A, *cpu->H);
}

void opcode_0x95(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // SUB, (L, None)
    (*cpu->A) = cpu_sub8(cpu, *cpu->A, *cpu->L);
}

void opcode_0x96(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // SUB, ((HL), None)
    (*cpu->A) = cpu_sub8(cpu, *cpu->A, mmu_read_byte(mmu, rom, cpu->HL));
}

void opcode_0x97(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // SUB, (A, None)
    (*cpu->A) = cpu_sub8(cpu, *cpu->A, *cpu->A);
}

void opcode_0x98(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // SBC, (A, B)
    (*cpu->A) = cpu_subc8(cpu, *cpu->A, *cpu->B);
}

void opcode_0x99(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // SBC, (A, C)
    (*cpu->A) = cpu_subc8(cpu, *cpu->A, *cpu->C);
}

void opcode_0x9a(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // SBC, (A, D)
    (*cpu->A) = cpu_subc8(cpu, *cpu->A, *cpu->D);
}

void opcode_0x9b(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // SBC, (A, E)
    (*cpu->A) = cpu_subc8(cpu, *cpu->A, *cpu->E);
}

void opcode_0x9c(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // SBC, (A, H)
    (*cpu->A) = cpu_subc8(cpu, *cpu->A, *cpu->H);
}

void opcode_0x9d(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // SBC, (A, L)
    (*cpu->A) = cpu_subc8(cpu, *cpu->A, *cpu->L);
}

void opcode_0x9e(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // SBC, (A, (HL))
    (*cpu->A) = cpu_subc8(cpu, *cpu->A, mmu_read_byte(mmu, rom, cpu->HL));
}

void opcode_0x9f(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // SBC, (A, A)
    (*cpu->A) = cpu_subc8(cpu, *cpu->A, *cpu->A);
}

void opcode_0xa0(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // AND, (B, None)
    (*cpu->A) = cpu_and8(cpu, *cpu->A, *cpu->B);
}

void opcode_0xa1(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // AND, (C, None)
    (*cpu->A) = cpu_and8(cpu, *cpu->A, *cpu->C);
}

void opcode_0xa2(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // AND, (D, None)
    (*cpu->A) = cpu_and8(cpu, *cpu->A, *cpu->D);
}

void opcode_0xa3(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // AND, (E, None)
    (*cpu->A) = cpu_and8(cpu, *cpu->A, *cpu->E);
}

void opcode_0xa4(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // AND, (H, None)
    (*cpu->A) = cpu_and8(cpu, *cpu->A, *cpu->H);
}

void opcode_0xa5(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // AND, (L, None)
    (*cpu->A) = cpu_and8(cpu, *cpu->A, *cpu->L);
}

void opcode_0xa6(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // AND, ((HL), None)
    (*cpu->A) = cpu_and8(cpu, *cpu->A, mmu_read_byte(mmu, rom, cpu->HL));
}

void opcode_0xa7(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // AND, (A, None)
    (*cpu->A) = cpu_and8(cpu, *cpu->A, *cpu->A);
}

void opcode_0xa8(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // XOR, (B, None)
    (*cpu->A) = cpu_xor8(cpu, *cpu->A, *cpu->B);
}

void opcode_0xa9(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // XOR, (C, None)
    (*cpu->A) = cpu_xor8(cpu, *cpu->A, *cpu->C);
}

void opcode_0xaa(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // XOR, (D, None)
    (*cpu->A) = cpu_xor8(cpu, *cpu->A, *cpu->D);
}

void opcode_0xab(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // XOR, (E, None)
    (*cpu->A) = cpu_xor8(cpu, *cpu->A, *cpu->E);
}

void opcode_0xac(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // XOR, (H, None)
    (*cpu->A) = cpu_xor8(cpu, *cpu->A, *cpu->H);
}

void opcode_0xad(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // XOR, (L, None)
    (*cpu->A) = cpu_xor8(cpu, *cpu->A, *cpu->L);
}

void opcode_0xae(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // XOR, ((HL), None)
    (*cpu->A) = cpu_xor8(cpu, *cpu->A, mmu_read_byte(mmu, rom, cpu->HL));
}

void opcode_0xaf(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // XOR, (A, None)
    (*cpu->A) = cpu_xor8(cpu, *cpu->A, *cpu->A);
}

void opcode_0xb0(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // OR, (B, None)
    (*cpu->A) = cpu_or8(cpu, *cpu->A, *cpu->B);
}

void opcode_0xb1(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // OR, (C, None)
    (*cpu->A) = cpu_or8(cpu, *cpu->A, *cpu->C);
}

void opcode_0xb2(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // OR, (D, None)
    (*cpu->A) = cpu_or8(cpu, *cpu->A, *cpu->D);
}

void opcode_0xb3(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // OR, (E, None)
    (*cpu->A) = cpu_or8(cpu, *cpu->A, *cpu->E);
}

void opcode_0xb4(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // OR, (H, None)
    (*cpu->A) = cpu_or8(cpu, *cpu->A, *cpu->H);
}

void opcode_0xb5(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // OR, (L, None)
    (*cpu->A) = cpu_or8(cpu, *cpu->A, *cpu->L);
}

void opcode_0xb6(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // OR, ((HL), None)
    (*cpu->A) = cpu_or8(cpu, *cpu->A, mmu_read_byte(mmu, rom, cpu->HL));
}

void opcode_0xb7(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // OR, (A, None)
    (*cpu->A) = cpu_or8(cpu, *cpu->A, *cpu->A);
}

void opcode_0xb8(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // CP, (B, None)
    cpu_cp8(cpu, *cpu->A, *cpu->B);
}

void opcode_0xb9(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // CP, (C, None)
    cpu_cp8(cpu, *cpu->A, *cpu->C);
}

void opcode_0xba(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // CP, (D, None)
    cpu_cp8(cpu, *cpu->A, *cpu->D);
}

void opcode_0xbb(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // CP, (E, None)
    cpu_cp8(cpu, *cpu->A, *cpu->E);
}

void opcode_0xbc(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // CP, (H, None)
    cpu_cp8(cpu, *cpu->A, *cpu->H);
}

void opcode_0xbd(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // CP, (L, None)
    cpu_cp8(cpu, *cpu->A, *cpu->L);
}

void opcode_0xbe(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // CP, ((HL), None)
    cpu_cp8(cpu, *cpu->A, mmu_read_byte(mmu, rom, cpu->HL));
}

void opcode_0xbf(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // CP, (A, None)
    cpu_cp8(cpu, *cpu->A, *cpu->A);
}

void opcode_0xc0(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // RET, (NZ, None)
    if(!cpu->FZ)
    {
        cpu->PC = stack_pop16(cpu, mmu, rom) - instruction_bytes[0xc0];
        cpu->ticks += 20;
    }
    else
    {
        cpu->ticks += 8;
    }
}

void opcode_0xc1(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // POP, (BC, None)
    cpu->BC = stack_pop16(cpu, mmu, rom);
}

void opcode_0xc2(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // JP, (NZ, a16)
    if(!cpu->FZ)
    {
        cpu->PC = get_operand_16(cpu, mmu, rom) - instruction_bytes[0xc2];
        cpu->ticks += 16;
    }
    else
    {
        cpu->ticks += 12;
    }
}

void opcode_0xc3(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // JP, (a16, None)
    cpu->PC = get_operand_16(cpu, mmu, rom) - instruction_bytes[0xc3];
}

void opcode_0xc4(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // CALL, (NZ, a16)
    if(!cpu->FZ)
    {
        uint16_t op = get_operand_16(cpu, mmu, rom);
        stack_push16(cpu, mmu, rom, cpu->PC + 3);
        cpu->PC = op;// - instruction_bytes[0xc4];
        cpu->ticks += 24;
    }
    else
    {
        cpu->ticks += 12;
    }
}

void opcode_0xc5(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // PUSH, (BC, None)
    stack_push16(cpu, mmu, rom, cpu->BC);
}

void opcode_0xc6(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // ADD, (A, d8)
    uint8_t op = get_first_operand_8(cpu, mmu, rom);
    (*cpu->A) = cpu_add8(cpu, *cpu->A, op);
}

void opcode_0xc7(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // RST, (00H, None)
    stack_push16(cpu, mmu, rom, cpu->PC + 1);
    cpu->PC = 0 - instruction_bytes[0xc7];
}

void opcode_0xc8(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // RET, (Z, None)
    if(cpu->FZ)
    {
        cpu->PC = stack_pop16(cpu, mmu, rom) - instruction_bytes[0xc8];
        cpu->ticks += 20;
    }
    else
    {
        cpu->ticks += 8;
    }
}

void opcode_0xc9(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // RET, (None, None)
    uint16_t sv = stack_pop16(cpu, mmu, rom);
    cpu->PC = sv - instruction_bytes[0xc9];
}

void opcode_0xca(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // JP, (Z, a16)
    if(cpu->FZ)
    {
        cpu->PC = get_operand_16(cpu, mmu, rom) - instruction_bytes[0xca];
        cpu->ticks += 16;
    }
    else
    {
        cpu->ticks += 12;
    }
}

void opcode_0xcb(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // PREFIX, (CB, None)
    uint8_t instruction = get_first_operand_8(cpu, mmu, rom);
    switch(instruction)
    {
        case 0x19: // RR, C (rotate right through carry)
        {
            bool new_carry = carry_rotr8(*cpu->C);
            (*cpu->C) = (*cpu->C) >> 1;
            if(cpu->FC)
                (*cpu->C) |= 1 << 7;
            cpu->FC = new_carry;
            cpu->FZ = (*cpu->C) == 0;
            cpu->FN = 0;
            cpu->FH = 0;
        }
            break;
        case 0x1A: // RR, D (rotate right through carry)
        {
            bool new_carry = carry_rotr8(*cpu->D);
            (*cpu->D) = (*cpu->D) >> 1;
            if(cpu->FC)
                (*cpu->D) |= 1 << 7;
            cpu->FC = new_carry;
            cpu->FZ = (*cpu->D) == 0;
            cpu->FN = 0;
            cpu->FH = 0;
        }
            break;
        case 0x27: // SLA, a (Shift left into carry)
            cpu->FC = carry_arith_shiftl8(*cpu->A);
            (*cpu->A) = (*cpu->A) << 1;
            cpu->FZ = (*cpu->A) == 0;
            cpu->FN = 0;
            cpu->FH = 0;
            break;
        case 0x33: // SWAP, e
            (*cpu->E) = ((*cpu->E) >> 4) | ((*cpu->E) << 4);
            
            cpu->FZ = (*cpu->E) == 0;
            cpu->FC = 0;
            cpu->FN = 0;
            cpu->FH = 0;
            break;
        case 0x37: // SWAP, a
            (*cpu->A) = ((*cpu->A) >> 4) | ((*cpu->A) << 4);
            
            cpu->FZ = (*cpu->A) == 0;
            cpu->FC = 0;
            cpu->FN = 0;
            cpu->FH = 0;
            break;
        case 0x38: // SRL, b (Shift right into carry)
            cpu->FC = carry_logic_shiftr8(*cpu->B);
            (*cpu->B) = (*cpu->B) >> 1;
            cpu->FZ = (*cpu->B) == 0;
            cpu->FN = 0;
            cpu->FH = 0;
            break;
        case 0x3F: // SRL, a (Shift right into carry)
            cpu->FC = carry_logic_shiftr8(*cpu->A);
            (*cpu->A) = (*cpu->A) >> 1;
            cpu->FZ = (*cpu->A) == 0;
            cpu->FN = 0;
            cpu->FH = 0;
            break;
        case 0x40: // BIT 0, b
            cpu_bit(cpu, 1, *cpu->B);
            break;
        case 0x41: // BIT 0, C
            cpu_bit(cpu, 1, *cpu->C);
            break;
        case 0x47: // BIT 0, a
            cpu_bit(cpu, 1 << 1, *cpu->A);
            break;
        case 0x48: // BIT 1, b
            cpu_bit(cpu, 1 << 1, *cpu->B);
            break;
        case 0x50: // BIT 2, b
            cpu_bit(cpu, 1 << 2, *cpu->B);
            break;
        case 0x57: // BIT 2, 1
            cpu_bit(cpu, 1 << 2, *cpu->A);
            break;
        case 0x58: // BIT 3, b
            cpu_bit(cpu, 1 << 3, *cpu->B);
            break;
        case 0x5F: // BIT 3, A
            cpu_bit(cpu, 1 << 3, *cpu->A);
            break;
        case 0x60: // BIT 4, b
            cpu_bit(cpu, 1 << 4, *cpu->B);
            break;
        case 0x61: // BIT 4, c
            cpu_bit(cpu, 1 << 4, *cpu->C);
            break;
        case 0x68: // BIT 5, b
            cpu_bit(cpu, 1 << 5, *cpu->B);
            break;
        case 0x69: // BIT 5, C
            cpu_bit(cpu, 1 << 5, *cpu->C);
            break;
        case 0x6E: // BIT 5, (HL)
            cpu_bit(cpu, 1 << 5, mmu_read_byte(mmu, rom, cpu->HL));
            break;
        case 0x6F: // BIT 5, a
            cpu_bit(cpu, 1 << 5, *cpu->A);
            break;
        case 0x70: // BIT 6, b
            cpu_bit(cpu, 1 << 6, *cpu->B);
            break;
        case 0x71: // BIT 6, c
            cpu_bit(cpu, 1 << 6, *cpu->C);
            break;
        case 0x76: // BIT 6, (HL)
            cpu_bit(cpu, 1 << 6, mmu_read_byte(mmu, rom, cpu->HL));
            break;
        case 0x77: // BIT 6, a
            cpu_bit(cpu, 1 << 6, *cpu->A);
            break;
        case 0x78: // BIT 7, b
            cpu_bit(cpu, 1 << 7, *cpu->B);
            break;
        case 0x79: // BIT 7, c
            cpu_bit(cpu, 1 << 7, *cpu->C);
            break;
        case 0x7f: // BIT 7, a
            cpu_bit(cpu, 1 << 7, *cpu->A);
            break;
        case 0x7e: // BIT 7, (HL)
            cpu_bit(cpu, 1 << 7, mmu_read_byte(mmu, rom, cpu->HL));
            break;
        case 0x86: // RES, (0, (HL))
        {
            uint8_t r = mmu_read_byte(mmu, rom, cpu->HL);
            r = r & ~(1);
            mmu_write_byte(mmu, rom, cpu->HL, r);
        }
            break;
        case 0x87: // RES, (0, a)
            (*cpu->A) = (*cpu->A) & 0xFE;
            break;
        case 0x9E: // RES, (3, (HL))
        {
            uint8_t r = mmu_read_byte(mmu, rom, cpu->HL);
            r = r & ~(1 << 3);
            mmu_write_byte(mmu, rom, cpu->HL, r);
        }
            break;
        case 0xBE: // RES, (7, (HL))
        {
            uint8_t r = mmu_read_byte(mmu, rom, cpu->HL);
            r = r & ~(1 << 7);
            mmu_write_byte(mmu, rom, cpu->HL, r);
        }
            break;
        case 0xDE: // SET, (3, (HL))
            mmu_write_byte(mmu, rom, cpu->HL, (mmu_read_byte(mmu, rom, cpu->HL) | 1 << 3));
            break;
        case 0xFE: // SET, (7, (HL))
            mmu_write_byte(mmu, rom, cpu->HL, (mmu_read_byte(mmu, rom, cpu->HL) | 1 << 7));
            break;
        default:
            std::cout << "0xcb: PREFIX not implemented!" << std::endl;
            break;
    }
    cpu->ticks += extended_instruction_ticks[instruction];
}

void opcode_0xcc(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // CALL, (Z, a16)
    if(cpu->FZ)
    {
        uint16_t op = get_operand_16(cpu, mmu, rom);
        stack_push16(cpu, mmu, rom, cpu->PC + 3);
        cpu->PC = op;// - instruction_bytes[0xcc];
        cpu->ticks += 24;
    }
    else
    {
        cpu->ticks += 12;
    }
}

void opcode_0xcd(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // CALL, (a16, None)
    uint16_t op = get_operand_16(cpu, mmu, rom);
    stack_push16(cpu, mmu, rom, cpu->PC + 3);
    //stack_push16(cpu, mmu, rom, cpu->PC);
    uint16_t sv = mmu_read_word(mmu, rom, cpu->SP);
    cpu->PC = op - instruction_bytes[0xcd];
}

void opcode_0xce(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // ADC, (A, d8)
    uint8_t op = get_first_operand_8(cpu, mmu, rom);
    (*cpu->A) = cpu_addc8(cpu, *cpu->A, op);
}

void opcode_0xcf(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // RST, (08H, None)
    stack_push16(cpu, mmu, rom, cpu->PC + 1);
    cpu->PC = 0x8 - instruction_bytes[0xcf];
}

void opcode_0xd0(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // RET, (NC, None)
    if(!cpu->FC)
    {
        cpu->PC = stack_pop16(cpu, mmu, rom) - instruction_bytes[0xd0];
        cpu->ticks += 20;
    }
    else
    {
        cpu->ticks += 8;
    }
}

void opcode_0xd1(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // POP, (DE, None)
    cpu->DE = stack_pop16(cpu, mmu, rom);
}

void opcode_0xd2(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // JP, (NC, a16)
    if(!cpu->FC)
    {
        cpu->PC = get_operand_16(cpu, mmu, rom) - instruction_bytes[0xd2];
        cpu->ticks += 16;
    }
    else
    {
        cpu->ticks += 12;
    }
}

void opcode_0xd3(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // NOP, (0, 0)
}

void opcode_0xd4(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // CALL, (NC, a16)
    if(!cpu->FC)
    {
        uint16_t op = get_operand_16(cpu, mmu, rom);
        stack_push16(cpu, mmu, rom, cpu->PC + 3);
        cpu->PC = op;// - instruction_bytes[0xd4];
        cpu->ticks += 24;
    }
    else
    {
        cpu->ticks += 12;
    }
}

void opcode_0xd5(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // PUSH, (DE, None)
    stack_push16(cpu, mmu, rom, cpu->DE);
}

void opcode_0xd6(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // SUB, (d8, None)
    uint8_t op = get_first_operand_8(cpu, mmu, rom);
    (*cpu->A) = cpu_sub8(cpu, *cpu->A, op);
}

void opcode_0xd7(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // RST, (10H, None)
    stack_push16(cpu, mmu, rom, cpu->PC + 1);
    cpu->PC = 0x10 - instruction_bytes[0xd7];
}

void opcode_0xd8(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // RET, (C, None)
    if(cpu->C)
    {
        cpu->PC = stack_pop16(cpu, mmu, rom) - instruction_bytes[0xd8];
        cpu->ticks += 20;
    }
    else
    {
        cpu->ticks += 8;
    }
}

void opcode_0xd9(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // RETI, (None, None)
    cpu->PC = stack_pop16(cpu, mmu, rom);
    cpu->PC -= instruction_bytes[0xd9];
    
    //cpu->PC = stack_pop16(cpu, mmu, rom);
    //cpu->PC -= instruction_bytes[mmu_read_byte(mmu, rom, cpu->PC)];
    cpu->interrupt.master_enabled = true;
}

void opcode_0xda(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // JP, (C, a16)
    if(cpu->FC)
    {
        cpu->PC = get_operand_16(cpu, mmu, rom) - instruction_bytes[0xda];
        cpu->ticks += 16;
    }
    else
    {
        cpu->ticks += 12;
    }
}

void opcode_0xdb(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // NOP, (0, 0)
}

void opcode_0xdc(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // CALL, (C, a16)
    if(cpu->FC)
    {
        uint16_t op = get_operand_16(cpu, mmu, rom);
        stack_push16(cpu, mmu, rom, cpu->PC + 3);
        cpu->PC = op;// - instruction_bytes[0xdc];
        cpu->ticks += 24;
    }
    else
    {
        cpu->ticks += 12;
    }
}

void opcode_0xdd(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // NOP, (0, 0)
}

void opcode_0xde(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // SBC, (A, d8)
    uint8_t op = get_first_operand_8(cpu, mmu, rom);
    (*cpu->A) = cpu_subc8(cpu, *cpu->A, op);
}

void opcode_0xdf(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // RST, (18H, None)
    stack_push16(cpu, mmu, rom, cpu->PC - 1);
    cpu->PC = 0x18 - instruction_bytes[0xdf];
}

void opcode_0xe0(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LDH, ((a8), A)
    uint16_t op = 0xff00 + (uint16_t)get_first_operand_8(cpu, mmu, rom);
    mmu_write_byte(mmu, rom, op, *cpu->A);
}

void opcode_0xe1(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // POP, (HL, None)
    cpu->HL = stack_pop16(cpu, mmu, rom);
}

void opcode_0xe2(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, ((C), A)
    mmu_write_byte(mmu, rom, 0xFF00 + *cpu->C, *cpu->A);
}

void opcode_0xe3(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // NOP, (0, 0)
}

void opcode_0xe4(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // NOP, (0, 0)
}

void opcode_0xe5(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // PUSH, (HL, None)
    stack_push16(cpu, mmu, rom, cpu->HL);
}

void opcode_0xe6(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // AND, (d8, None)
    uint8_t op = get_first_operand_8(cpu, mmu, rom);
    (*cpu->A) = cpu_and8(cpu, *cpu->A, op);
}

void opcode_0xe7(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // RST, (20H, None)
    stack_push16(cpu, mmu, rom, cpu->PC + 1);
    cpu->PC = 0x20 - instruction_bytes[0xe7];
}

void opcode_0xe8(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // ADD, (SP, r8)
    uint8_t op = get_first_operand_8(cpu, mmu, rom);
    cpu->SP = cpu_add16(cpu, cpu->SP, (uint16_t)op);
}

void opcode_0xe9(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // JP, ((HL), None)
    // incorrect?
    //cpu->PC = mmu_read_word(mmu, rom, cpu->HL) - instruction_bytes[0xe9];
    cpu->PC = cpu->HL - instruction_bytes[0xe9];
}

void opcode_0xea(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, ((a16), A)
    uint16_t op = get_operand_16(cpu, mmu, rom);
    mmu_write_byte(mmu, rom, op, *cpu->A);
}

void opcode_0xeb(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // NOP, (0, 0)
}

void opcode_0xec(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // NOP, (0, 0)
}

void opcode_0xed(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // NOP, (0, 0)
}

void opcode_0xee(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // XOR, (d8, None)
    uint8_t op = get_first_operand_8(cpu, mmu, rom);
    (*cpu->A) = cpu_xor8(cpu, *cpu->A, op);
}

void opcode_0xef(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // RST, (28H, None)
    stack_push16(cpu, mmu, rom, cpu->PC + 1);
    cpu->PC = 0x28 - instruction_bytes[0xef];
}

void opcode_0xf0(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LDH, (A, (a8))
    uint16_t op = 0xff00 + (uint16_t)get_first_operand_8(cpu, mmu, rom);
    (*cpu->A) = mmu_read_byte(mmu, rom, op);
}

void opcode_0xf1(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // POP, (AF, None)
    cpu->AF = stack_pop16(cpu, mmu, rom);
}

void opcode_0xf2(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (A, (C))
    (*cpu->A) = mmu_read_byte(mmu, rom, *cpu->C);
}

void opcode_0xf3(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // DI, (None, None)
    // TODO: DISABLE INTERRUPTS
    cpu->interrupt.master_enabled = false;
}

void opcode_0xf4(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // NOP, (0, 0)
}

void opcode_0xf5(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // PUSH, (AF, None)
    stack_push16(cpu, mmu, rom, cpu->AF);
}

void opcode_0xf6(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // OR, (d8, None)
    uint8_t op = get_first_operand_8(cpu, mmu, rom);
    (*cpu->A) = cpu_or8(cpu, *cpu->A, op);
}

void opcode_0xf7(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // RST, (30H, None)
    stack_push16(cpu, mmu, rom, cpu->PC + 1);
    cpu->PC = 0x30 - instruction_bytes[0xf7];
}

void opcode_0xf8(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD16, (HL, SP+r8)
    uint8_t op = get_first_operand_8(cpu, mmu, rom);
    cpu->HL = cpu->SP + (int8_t)op;
    cpu->FN = false;
    cpu->FN = false;
}

void opcode_0xf9(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD16, (SP, HL)
    cpu->SP = cpu->HL;
}

void opcode_0xfa(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // LD8, (A, (a16))
    uint16_t op = get_operand_16(cpu, mmu, rom);
    (*cpu->A) = mmu_read_byte(mmu, rom, op);
}

void opcode_0xfb(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // EI, (None, None)
    // TODO: ENABLE INTERRUPTS
    cpu->interrupt.master_enabled = true;
}

void opcode_0xfc(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // NOP, (0, 0)
}

void opcode_0xfd(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // NOP, (0, 0)
}

void opcode_0xfe(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // CP, (d8, None)
    uint8_t op = get_first_operand_8(cpu, mmu, rom);
    cpu_cp8(cpu, *cpu->A, op);
}

void opcode_0xff(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    // RST, (38H, None)
    stack_push16(cpu, mmu, rom, cpu->PC + 1);
    cpu->PC = 0x38 - instruction_bytes[0xff];
}
