//
//  console.cpp
//  GameBoy
//
//  Created by Robin Carlsson on 2016-09-30.
//  Copyright © 2016 Robin Carlsson. All rights reserved.
//

#include "console.hpp"
#include "mmu.hpp"

Console::Console(Cartridge* rom) : rom(rom), keys(0)
{
    cpu_init(&this->cpu);
    mmu_init(&this->mmu);
    gpu_init(&this->gpu);
    
    cpu_init_defaults(&this->cpu, &this->mmu);
}

void console_step(Console* console)
{
    // Set contoller state
    /*uint8_t contoller_state = mmu_read_byte(&console->mmu, console->rom, 0xFF00);
    uint8_t stored_controller_state = mmu_read_byte(&console->mmu, console->rom, 0xFF80);
    if(stored_controller_state > 0)
    {
        
    }
    if(console->keys > 0)
    {
        if((contoller_state & 0x20) > 0)
            mmu_write_byte(&console->mmu, console->rom, 0xFF00, contoller_state | (console->keys & 0x0F));
        else if((contoller_state & 0x10) > 0)
            mmu_write_byte(&console->mmu, console->rom, 0xFF00, contoller_state | ((console->keys & 0xF0) >> 4));
        
        contoller_state = mmu_read_byte(&console->mmu, console->rom, 0xFF00);
    }*/
    
    //cpu_step(console);
    //gpu_step(&console->gpu, &console->cpu, &console->mmu, console->rom);
    //cpu_interrupt_step(&console->cpu, &console->gpu, &console->mmu, console->rom);
}
