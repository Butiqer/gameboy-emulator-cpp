//
//  console.hpp
//  GameBoy
//
//  Created by Robin Carlsson on 2016-09-30.
//  Copyright © 2016 Robin Carlsson. All rights reserved.
//

#pragma once

#define CONSOLE_KEYS_A      (1)
#define CONSOLE_KEYS_B      (1 << 1)
#define CONSOLE_KEYS_SELECT (1 << 2)
#define CONSOLE_KEYS_START  (1 << 3)

/*#define CONSOLE_KEYS_DOWN   (1 << 4)
#define CONSOLE_KEYS_UP     (1 << 5)
#define CONSOLE_KEYS_LEFT   (1 << 6)
#define CONSOLE_KEYS_RIGHT  (1 << 7)*/

#define CONSOLE_KEYS_RIGHT  (1)
#define CONSOLE_KEYS_LEFT   (1 << 1)
#define CONSOLE_KEYS_UP     (1 << 2)
#define CONSOLE_KEYS_DOWN   (1 << 3)



#include "cpu.hpp"
#include "mmu.hpp"
#include "romparser.hpp"
#include "gpu.hpp"

struct Console
{
    Console(Cartridge*);
    CPU cpu;
    MMU mmu;
    Cartridge* rom;
    GPU gpu;
    
    uint8_t keys;
};

void console_load_game(Console*, const char*);
void console_step(Console*);
