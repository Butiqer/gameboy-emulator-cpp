//
//  mmu.cpp
//  GameBoy
//
//  Created by Robin Carlsson on 2016-06-11.
//  Copyright © 2016 Robin Carlsson. All rights reserved.
//

#include <iostream>

#include "mmu.hpp"
#include "romparser.hpp"
#include "audio.hpp"

uint8_t keys1 = 0;
uint8_t keys2 = 0;

void mmu_init(MMU* mmu)
{
    mmu->ram_data   = new uint8_t[MMU_RAM_SIZE]   {0};
    mmu->gram_data  = new uint8_t[MMU_GRAM_SIZE]  {0};
    mmu->eram_data  = new uint8_t[MMU_ERAM_SIZE]  {0};
    mmu->oam_data   = new uint8_t[MMU_OAM_SIZE]   {0};
    mmu->hram_data  = new uint8_t[MMU_HRAM_SIZE]  {0};
    mmu->ioram_data = new uint8_t[MMU_IORAM_SIZE] {0};
    memcpy(mmu->ioram_data, MMU_IO_RESET, 0x100);
}

uint8_t mmu_read_byte(MMU* mmu, Cartridge* rom, uint16_t address)
{
    if(address < 0x8000) // Cartridge ROM
    {
        return rom->data[address];
    }
    else if (address < 0xA000) // Graphics RAM
    {
        //std::cout << "Reading from gram" << std::endl;
        return mmu->gram_data[address - 0x8000];
    }
    else if (address < 0xC000) // Cartridge RAM
    {
        return mmu->eram_data[address - 0xA000];
    }
    else if (address < 0xE000) // RAM
    {
        return mmu->ram_data[address - 0xC000];
    }
    else if (address < 0xFE00) // RAM Shadow
    {
        return mmu->ram_data[address - 0xC000 - 0x2000];
    }
    //else if (address < 0xFEFF) // Sprite RAM
    else if (address < 0xFEA0) // Sprite RAM
    {
        return mmu->oam_data[address - 0xFE00];
        //std::cout << "error: sprite RAM not implemented" << std::endl;
    }
    else if (address < 0xFF00) {}
    else if (address < 0xFF80) // IO
    {
        if(address == 0xff00)
        {
            
            if(!(mmu->ioram_data[0x00] & 0x20)) {
                return (uint8_t)(0xc0 | ~(keys1) | 0x10);
                //uint8_t keys = (rand()) % 4;
                //return (uint8_t)((~(0x01 << keys) & 0x0F) | 0x10);
            }
            
            else if(!(mmu->ioram_data[0x00] & 0x10)) {
                //uint8_t keys = (rand()) % 4;
                //uint8_t keys = 0;
                //return (uint8_t)((~(0x01 << keys) & 0x0F) | 0x20);
                if(keys2 > 0)
                {
                    
                }
                return (uint8_t)(0xc0 | ~(keys2) | 0x20);
                //return 0x2F;
                
                
                //return (mmu->ioram_data[0x00] & 0x0F) | 0xc0 | 0x20;
            }
            
            if((mmu->ioram_data[0x00] & 0x30) == 0x30)
            {
                return 0xff;
            }
            else
            {
                
            }
            //return mmu->ioram_data[0x00];
        }
        else if(address == 0xFF04)
        {
            return (uint8_t)rand();
        }
        //std::cout << "read error: memory mapped I/O not implemented" << std::endl;
        
        return mmu->ioram_data[address - 0xFF00];
    }
    else if (address <= 0xFFFF) // Zero-page RAM
    {
        if(address == 0xff0f)
        {
            
        }
        if(address == 0xff80)
        {
            //return 0;
        }
        return mmu->hram_data[address - 0xFF80];
    }
    return 0;
}

uint16_t mmu_read_word(MMU* mmu, Cartridge* rom, uint16_t address)
{
    uint8_t a1 = mmu_read_byte(mmu, rom, address);
    uint8_t a2 = mmu_read_byte(mmu, rom, address + 1);
    return (uint16_t)a1 + (((uint16_t)a2) << 8);
    //return (uint16_t) + (uint16_t)(((uint16_t)mmu_read_byte(mmu, rom, address + 1)) << 8);
}

void mmu_write_byte(MMU* mmu, Cartridge* rom, uint16_t address, uint8_t value)
{
    if(address < 0x8000) // Cartridge ROM
    {
        //return rom->data[address];
        //throw "";
        std::cout << "error: trying to write to rom" << std::endl;
    }
    else if (address < 0xA000) // Graphics RAM
    {
        mmu->gram_data[address - 0x8000] = value;
        //std::cout << "Writing to gram" << (int)value << " " << std::endl;
    }
    else if (address < 0xC000) // Cartridge RAM
    {
        mmu->eram_data[address - 0xA000] = value;
    }
    else if (address < 0xE000) // RAM
    {
        mmu->ram_data[address - 0xC000] = value;
    }
    else if (address < 0xFE00) // RAM Shadow
    {
        mmu->ram_data[address - 0xC000 - 0x2000] = value;
        //std::cout << "error: working RAM not implemented" << std::endl;
    }
    //else if (address < 0xFEFF) // Sprite RAM
    else if (address < 0xFEA0) // Sprite RAM
    {
        //std::cout << "error: sprite RAM not implemented" << std::endl;
        
        mmu->oam_data[address - 0xFE00] = value;
    }
    else if (address < 0xFF00)
    {
        //std::cout << "error: sprite RAM not implemented" << std::endl;
        
    }
    else if (address < 0xFF80) // IO
    {
        if(address == 0xff00 && (value & 0x0f) != 0)
        {
            
        }
        if(address == 0xFF46)
        {
            //copy(0xfe00, value << 8, 160); // OAM DMA
            mmu_copy(mmu, rom, value << 8, 0xFE00, 160);
            
        }
        else
        {
            if(address == 0xff13 || address == 0xff14)
            {
                
            }
            mmu->ioram_data[address - 0xFF00] = value;
        }
    }
    else if (address <= 0xFFFF) // High RAM
    {
        
        if(address == 0xffff && value > 0)
        {
            
        }
        if(address == 0xff80 && value > 0)
        {
            
        }
        //if(address == 0xff80)
        //    return;
        mmu->hram_data[address - 0xFF80] = value;
    }
    
    if (address == 0xFF02 && value == 0x81)
    {
        std::cout << value;
    }
}

void mmu_write_word(MMU* mmu, Cartridge* rom, uint16_t address, uint16_t value)
{
    mmu_write_byte(mmu, rom, address, value & 0x00ff);
    mmu_write_byte(mmu, rom, address + 1, (value & 0xff00) >> 8);
}

void mmu_copy(MMU* mmu, Cartridge* rom, uint16_t src, uint16_t dest, size_t size)
{
    for(uint16_t i = 0; i < size; i++)
        mmu_write_byte(mmu, rom, dest + i, mmu_read_byte(mmu, rom, src + i));
}

















