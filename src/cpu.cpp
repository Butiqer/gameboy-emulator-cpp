 //
//  cpu.cpp
//  GameBoy
//
//  Created by Robin Carlsson on 2016-06-01.
//  Copyright © 2016 Robin Carlsson. All rights reserved.
//

#include <iostream>
#include <iomanip>

#include "cpu.hpp"
#include "romparser.hpp"
#include "opcodes.h"
#include "mmu.hpp"
#include "console.hpp"

void cpu_init(CPU* cpu)
{
    cpu->F = (uint8_t*)(&cpu->AF);
    cpu->A = cpu->F + 1;
    
    cpu->C = (uint8_t*)(&cpu->BC);
    cpu->B = cpu->C + 1;
    
    cpu->E = (uint8_t*)(&cpu->DE);
    cpu->D = cpu->E + 1;
    
    cpu->L = (uint8_t*)(&cpu->HL);
    cpu->H = cpu->L + 1;
    
    cpu->ticks = 0;
}

void cpu_init_defaults(CPU* cpu, MMU* mmu)
{
    cpu->AF = 0x01B0;
    cpu->BC = 0x0013;
    cpu->DE = 0x00d8;
    cpu->HL = 0x014D;
    cpu->SP = 0xfffe;
    cpu->PC = 0x100;
    
    cpu->interrupt.master_enabled = true;
    cpu->interrupt.enabled = 0;
    cpu->interrupt.flags = 0;
    
    mmu->ram_data[0xFF05] = 0x00;
    mmu->ram_data[0xFF06] = 0x00;
    mmu->ram_data[0xFF07] = 0x00;
    mmu->ram_data[0xFF10] = 0x80;
    mmu->ram_data[0xFF11] = 0xBF;
    mmu->ram_data[0xFF12] = 0xF3;
    mmu->ram_data[0xFF14] = 0xBF;
    mmu->ram_data[0xFF16] = 0x3F;
    mmu->ram_data[0xFF17] = 0x00;
    mmu->ram_data[0xFF19] = 0xBF;
    mmu->ram_data[0xFF1A] = 0x7F;
    mmu->ram_data[0xFF1B] = 0xFF;
    mmu->ram_data[0xFF1C] = 0x9F;
    mmu->ram_data[0xFF1E] = 0xBF;
    mmu->ram_data[0xFF20] = 0xFF;
    mmu->ram_data[0xFF21] = 0x00;
    mmu->ram_data[0xFF22] = 0x00;
    mmu->ram_data[0xFF23] = 0xBF;
    mmu->ram_data[0xFF24] = 0x77;
    mmu->ram_data[0xFF25] = 0xF3;
    mmu->ram_data[0xFF26] = 0xF1;
    mmu->ram_data[0xFF40] = 0x91;
    mmu->ram_data[0xFF42] = 0x00;
    mmu->ram_data[0xFF43] = 0x00;
    mmu->ram_data[0xFF45] = 0x00;
    mmu->ram_data[0xFF47] = 0xFC;
    mmu->ram_data[0xFF48] = 0xFF;
    mmu->ram_data[0xFF49] = 0xFF;
    mmu->ram_data[0xFF4A] = 0x00;
    mmu->ram_data[0xFF4B] = 0x00;
    mmu->ram_data[0xFFFF] = 0x00;
}

void cpu_step(Console* console)
{
    uint8_t instruction = mmu_read_byte(&console->mmu, console->rom, console->cpu.PC);
    /*std::cout << std::hex << console->cpu.PC << " " << (int)instruction_bytes[instruction] << " " << (int)instruction << " ";
    for(int i = 0; i < instruction_bytes[instruction] - 1; i++)
        std::cout << std::hex << (int)mmu_read_byte(&console->mmu, console->rom, console->cpu.PC + i + 1) << " ";
    std::cout << std::endl;
    */
    (*opcode_funcs[instruction])(&console->cpu, &console->mmu, console->rom);
    console->cpu.PC += instruction_bytes[instruction];
    console->cpu.ticks += instruction_ticks[instruction];
    cpu_update_flag_register(&console->cpu);


    /*if(console->cpu.PC == 0x6a13)
    {
        cpu_print_stack(&console->cpu, &console->mmu, console->rom);
        cpu_dump_memory(&console->cpu, &console->mmu, console->rom, 0x0, 0x100);
    }*/
}

void cpu_update_flags(CPU* cpu, bool FZ, bool FN, bool FH, bool FC)
{
    cpu->FZ = FZ;
    cpu->FN = FN;
    cpu->FH = FH;
    cpu->FC = FC;
}

void cpu_update_flag_register(CPU* cpu)
{
    (*cpu->F) = 0;
    (*cpu->F) |= (uint8_t)cpu->FZ << 7;
    (*cpu->F) |= (uint8_t)cpu->FN << 6;
    (*cpu->F) |= (uint8_t)cpu->FH << 5;
    (*cpu->F) |= (uint8_t)cpu->FC << 4;
}

void cpu_interrupt_step(CPU* cpu, GPU* gpu, MMU* mmu, Cartridge* rom)
{
    cpu->interrupt.flags = mmu_read_byte(mmu, rom, 0xFF0F);
    cpu->interrupt.enabled = mmu_read_byte(mmu, rom, 0xFFFF);
    
    if(!cpu->interrupt.master_enabled)
        return;
    
    if(cpu->interrupt.enabled & cpu->interrupt.flags & CPU_INTERRUPT_VBLANK)
    {
        cpu->interrupt.flags &= ~CPU_INTERRUPT_VBLANK;
        
        cpu->interrupt.master_enabled = false;
        
        stack_push16(cpu, mmu, rom, cpu->PC);
        cpu->PC = 0x40;
        
        cpu->ticks += 12;
    }
    if(cpu->interrupt.enabled & cpu->interrupt.flags & CPU_INTERRUPT_LCDSTAT)
    {
        cpu->interrupt.flags &= ~CPU_INTERRUPT_LCDSTAT;
        
        cpu->interrupt.master_enabled = false;
        stack_push16(cpu, mmu, rom, cpu->PC);
        cpu->PC = 0x48;
        
        cpu->ticks += 12;
    }
    if(cpu->interrupt.enabled & cpu->interrupt.flags & CPU_INTERRUPT_TIMER)
    {
        cpu->interrupt.flags &= ~CPU_INTERRUPT_TIMER;
        
        cpu->interrupt.master_enabled = false;
        stack_push16(cpu, mmu, rom, cpu->PC);
        cpu->PC = 0x50;
        
        cpu->ticks += 12;
    }
    if(cpu->interrupt.enabled & cpu->interrupt.flags & CPU_INTERRUPT_SERIAL)
    {
        cpu->interrupt.flags &= ~CPU_INTERRUPT_SERIAL;
        
        cpu->interrupt.master_enabled = false;
        stack_push16(cpu, mmu, rom, cpu->PC);
        cpu->PC = 0x58;
        
        cpu->ticks += 12;
    }
    if(cpu->interrupt.enabled & cpu->interrupt.flags & CPU_INTERRUPT_JOYPAD)
    {
        cpu->interrupt.flags &= ~CPU_INTERRUPT_JOYPAD;
        
        cpu->interrupt.master_enabled = false;
        stack_push16(cpu, mmu, rom, cpu->PC);
        cpu->PC = 0x58;
        
        cpu->ticks += 12;
    }
    mmu_write_byte(mmu, rom, 0xFF0F, cpu->interrupt.flags);
    mmu_write_byte(mmu, rom, 0xFFFF, cpu->interrupt.enabled);
}

void cpu_print_stack(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    std::cout << "Stack" << std::endl;
    for(uint16_t i = cpu->SP; i <= 0xCFFF; i += 2)
    {
        std::cout << i << ": " << (uint16_t)mmu_read_word(mmu, rom, i) << std::endl;
    }
}

void cpu_dump_memory(CPU* cpu, MMU* mmu, Cartridge* rom, uint16_t start, uint16_t size)
{
    for(int i = start; i < start + size; i += 0x10)
    {
        std::cout << std::hex << std::setw(4) << i << " ";
        for(int j = 0; j < 0x10; j++)
        {
            std::cout << std::setw(2) << std::setfill('0') << (uint16_t)mmu_read_byte(mmu, rom, i + j);
        }
        
        /*std::cout << " ";
        for(int j = 0; j < 0x10; j++)
        {
            std::cout << std::setw(1) << (char)mmu_read_byte(mmu, rom, i + j);
        }*/
        
        std::cout << std::endl;
    }
}

uint16_t get_operand_16(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    //std::cout << "operand16 for instruction " << (int)mmu_read_byte(mmu, rom, cpu->PC) << ": " << (int)mmu_read_word(mmu, rom, cpu->PC + 1) << std::endl;
    return mmu_read_word(mmu, rom, cpu->PC + 1);
}

uint8_t get_first_operand_8(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    //std::cout << "operand8 for instruction " << (int)mmu_read_byte(mmu, rom, cpu->PC) << ": " << (int)mmu_read_byte(mmu, rom, cpu->PC + 1) << std::endl;
    return mmu_read_byte(mmu, rom, cpu->PC + 1);
}

uint16_t* get_register_pointer(CPU* cpu, uint8_t operand)
{
    switch(operand)
    {
        case 1: case 2:                    return &cpu->BC;
        case 5: case 15: case 21: case 22: return &cpu->HL;
        case 9: case 10:                   return &cpu->DE;
        case 34:                           return &cpu->AF;
        case 20:                           return &cpu->SP;
        
        case 6:           return (uint16_t*)cpu->A;
        case 3:           return (uint16_t*)cpu->B;
        case 7: case 31:  return (uint16_t*)cpu->C;
        case 11:          return (uint16_t*)cpu->D;
        case 13:          return (uint16_t*)cpu->E;
        case 16:          return (uint16_t*)cpu->H;
        case 18:          return (uint16_t*)cpu->L;
    }
    return nullptr;
}

uint8_t* get_register_operand8(CPU* cpu, uint8_t operand)
{
    switch(operand)
    {
        case 6:          return cpu->A;
        case 3:          return cpu->B;
        case 7: case 31: return cpu->C;
        case 11:         return cpu->D;
        case 13:         return cpu->E;
        case 16:         return cpu->H;
        case 18:         return cpu->L;
    }
    return nullptr;
}

uint16_t* get_register_operand16(CPU* cpu, uint8_t operand)
{
    switch(operand)
    {
        case 1: case 2:                    return &cpu->BC;
        case 5: case 15: case 21: case 22: return &cpu->HL;
        case 9: case 10:                   return &cpu->DE;
        case 34:                           return &cpu->AF;
        case 20:                           return &cpu->SP;
    }
    return nullptr;
}

uint8_t rotl8 (uint8_t n, unsigned int c)
{
    const unsigned int mask = (CHAR_BIT*sizeof(n)-1);
    
    //assert ( (c<=mask) &&"rotate by type width or more");
    c &= mask;  // avoid undef behaviour with NDEBUG.  0 overhead for most types / compilers
    return (n << c) | (n >> ((static_cast<uint8_t>(-static_cast<int8_t>(c))) & mask));
}

uint8_t rotr8 (uint8_t n, unsigned int c)
{
    const unsigned int mask = (CHAR_BIT*sizeof(n)-1);
    
    //assert ( (c<=mask) &&"rotate by type width or more");
    c &= mask;  // avoid undef behaviour with NDEBUG.  0 overhead for most types / compilers
    return (n >> c) | (n << ((static_cast<uint8_t>(-static_cast<int8_t>(c))) & mask ));
}

bool carry_rotl8(uint8_t n)
{
    const unsigned int mask = (CHAR_BIT*sizeof(n)-1);
    
    return (n & mask) != 0;
}

bool carry_rotr8(uint8_t n)
{
    const unsigned int mask = 0x1;
    
    return (n & mask) != 0;
}

bool carry_arith_shiftl8(uint8_t n)
{
    const unsigned int mask = 0xE;
    return (n & mask) != 0;
}

bool carry_logic_shiftr8(uint8_t n)
{
    const unsigned int mask = 0x1;
    return (n & mask) != 0;
}

bool carry_add8(uint8_t a, uint8_t b)
{
    uint8_t res = a + b;
    return res < a || res < b;
}

bool carry_add16(uint16_t a, uint16_t b)
{
    uint16_t res = a + b;
    return res < a || res < b;
}

bool carry_sub8(uint8_t a, uint8_t b)
{
    return a - b > a;
}

bool carry_sub16(uint16_t a, uint16_t b)
{
    return a - b > a;
}

bool carry_inchi8(uint8_t a)
{
    uint8_t res = a + 1;
    return res > 0x4 && a <= 0x4;
}

bool carry_inchi16(uint16_t a)
{
    uint16_t res = a + 1;
    return res > 0x400 && a <= 0x400;
}

bool carry_addhi8(uint8_t a, uint8_t b)
{
    uint16_t res = a + b;
    return (res > 0x4 && a <= 0x4) || (res < a && a <= 0x4) || (res < b && a <= 0x4);
}

bool carry_addhi16(uint16_t a, uint16_t b)
{
    uint16_t res = a + b;
    return (res > 0x400 && a <= 0x400) || (res < a && a <= 0x400) || (res < b && a <= 0x400);
}

bool carry_subhi8(uint8_t a, uint8_t b)
{
    return (a >= 0x4 && a - b < 0x4) || a - b > a;
}

bool carry_subhi16(uint16_t a, uint16_t b)
{
    return (a >= 0x400 && a - b < 0x400) || a - b > a;
}

bool carry_dechi8(uint8_t a)
{
    uint8_t res = a - 1;
    return res < 0x4 && a >= 0x4;
}

bool carry_dechi16(uint16_t a)
{
    uint16_t res = a - 1;
    return res < 0x400 && a >= 0x400;
}

uint8_t cpu_add8(CPU* cpu, uint8_t a, uint8_t b)
{
    bool carry_hi = carry_addhi8(a, b);
    bool carry = carry_add8(a, b);
    a += b;
    
    // Set flags
    cpu->FZ = a == 0;
    cpu->FN = false;
    cpu->FH = carry_hi;
    cpu->FC = carry;
    
    return a;
}

uint16_t cpu_add16(CPU* cpu, uint16_t a, uint16_t b)
{
    bool carry_hi = carry_addhi16(a, b);
    bool carry = carry_add16(a, b);
    a += b;
    
    // Set flags
    cpu->FZ = a == 0;
    cpu->FN = false;
    cpu->FH = carry_hi;
    cpu->FC = carry;
    
    return a;
}

uint8_t cpu_addc8(CPU* cpu, uint8_t a, uint8_t b)
{
    bool carry_hi = carry_addhi8(a, b);
    bool carry = carry_add8(a, b);
    a += b + (uint8_t)cpu->FC;
    
    // Set flags
    cpu->FZ = a == 0;
    cpu->FN = false;
    cpu->FH = carry_hi;
    cpu->FC = carry;
    
    return a;
}

uint16_t cpu_addc16(CPU* cpu, uint16_t a, uint16_t b)
{
    bool carry_hi = carry_addhi16(a, b);
    bool carry = carry_add16(a, b);
    a += b + (uint16_t)cpu->FC;
    
    // Set flags
    cpu->FZ = a == 0;
    cpu->FN = false;
    cpu->FH = carry_hi;
    cpu->FC = carry;
    
    return a;
}

uint8_t cpu_sub8(CPU* cpu, uint8_t a, uint8_t b)
{
    bool carry_hi = carry_subhi8(a, b);
    bool carry = carry_sub8(a, b);
    a -= b;
    
    // Set flags
    cpu->FZ = a == 0;
    cpu->FN = true;
    cpu->FH = !carry_hi;
    cpu->FC = !carry;
    
    return a;
}

uint16_t cpu_sub16(CPU* cpu, uint16_t a, uint16_t b)
{
    bool carry_hi = carry_subhi16(a, b);
    bool carry = carry_sub16(a, b);
    a -= b;
    
    // Set flags
    cpu->FZ = a == 0;
    cpu->FN = true;
    cpu->FH = !carry_hi;
    cpu->FC = !carry;
    
    return a;
}

uint8_t cpu_subc8(CPU* cpu, uint8_t a, uint8_t b)
{
    bool carry_hi = carry_subhi8(a, b);
    bool carry = carry_sub8(a, b);
    a -= b + (uint8_t)cpu->FC;
    
    // Set flags
    cpu->FZ = a == 0;
    cpu->FN = true;
    cpu->FH = !carry_hi;
    cpu->FC = !carry;
    
    return a;
}

uint16_t cpu_subc16(CPU* cpu, uint16_t a, uint16_t b)
{
    bool carry_hi = carry_subhi16(a, b);
    bool carry = carry_sub16(a, b);
    a -= b + (uint16_t)cpu->FC;
    
    // Set flags
    cpu->FZ = a == 0;
    cpu->FN = true;
    cpu->FH = !carry_hi;
    cpu->FC = !carry;
    
    return a;
}

uint8_t cpu_inc8(CPU* cpu, uint8_t n)
{
    bool carry_hi = carry_inchi8(n);
    n++;
    
    cpu->FZ = n == 0;
    cpu->FN = false;
    cpu->FH = carry_hi;
    
    return n;
}

uint16_t cpu_inc16(CPU* cpu, uint16_t n)
{
    bool carry_hi = carry_inchi16(n);
    n++;
    
    cpu->FZ = n == 0;
    cpu->FN = false;
    cpu->FH = carry_hi;
    
    return n;
}

uint8_t cpu_dec8(CPU* cpu, uint8_t n)
{
    bool carry_hi = carry_dechi8(n);
    n--;
    
    cpu->FZ = n == 0;
    cpu->FN = true;
    cpu->FH = !carry_hi;
    
    return n;
}

uint16_t cpu_dec16(CPU* cpu, uint16_t n)
{
    bool carry_hi = carry_dechi16(n);
    n--;
    
    cpu->FZ = n == 0;
    cpu->FN = true;
    cpu->FH = !carry_hi;
    
    return n;
}

uint8_t cpu_and8(CPU* cpu, uint8_t a, uint8_t b)
{
    a = a & b;
    
    cpu->FZ = a == 0;
    cpu->FN = false;
    cpu->FH = true;
    cpu->FC = false;
    
    return a;
}

uint16_t cpu_and16(CPU* cpu, uint16_t a, uint16_t b)
{
    a = a & b;
    
    cpu->FZ = a == 0;
    cpu->FN = false;
    cpu->FH = true;
    cpu->FC = false;
    
    return a;
}

uint8_t cpu_xor8(CPU* cpu, uint8_t a, uint8_t b)
{
    a = a ^ b;
    
    cpu->FZ = a == 0;
    cpu->FN = false;
    cpu->FH = false;
    cpu->FC = false;
    
    return a;
}

uint16_t cpu_xor16(CPU* cpu, uint16_t a, uint16_t b)
{
    a = a ^ b;
    
    cpu->FZ = a == 0;
    cpu->FN = false;
    cpu->FH = false;
    cpu->FC = false;
    
    return a;
}

uint8_t cpu_or8(CPU* cpu, uint8_t a, uint8_t b)
{
    a = a | b;
    
    cpu->FZ = a == 0;
    cpu->FN = false;
    cpu->FH = false;
    cpu->FC = false;
    
    return a;
}

uint16_t cpu_or16(CPU* cpu, uint16_t a, uint16_t b)
{
    a = a | b;
    
    cpu->FZ = a == 0;
    cpu->FN = false;
    cpu->FH = false;
    cpu->FC = false;
    
    return a;
}

void cpu_cp8(CPU* cpu, uint8_t a, uint8_t b)
{
    cpu->FZ = a == b;
    cpu->FN = true;
    cpu->FH = !carry_subhi8(a, b);
    cpu->FC = a < b;
}

void cpu_cp16(CPU* cpu, uint16_t a, uint16_t b)
{
    cpu->FZ = a == b;
    cpu->FN = true;
    cpu->FH = !carry_subhi16(a, b);
    cpu->FC = a < b;
}

void cpu_bit(CPU* cpu, uint8_t mask, uint8_t n)
{
    cpu->FZ = (mask & n) == 0;
    cpu->FN = false;
    cpu->FH = true;
}

uint8_t stack_pop8(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    uint8_t v = mmu_read_byte(mmu, rom, cpu->SP);
    cpu->SP++;
    return v;
}

uint16_t stack_pop16(CPU* cpu, MMU* mmu, Cartridge* rom)
{
    uint16_t v = mmu_read_word(mmu, rom, cpu->SP);
    cpu->SP++;
    cpu->SP++;
    return v;
}

void stack_push8(CPU* cpu, MMU* mmu, Cartridge* rom, uint8_t n)
{
    cpu->SP--;
    mmu_write_byte(mmu, rom, cpu->SP, n);
}

void stack_push16(CPU* cpu, MMU* mmu, Cartridge* rom, uint16_t n)
{
    cpu->SP--;
    cpu->SP--;
    if(n == 0x39)
    {
        
    }
    mmu_write_word(mmu, rom, cpu->SP, n);
}


























