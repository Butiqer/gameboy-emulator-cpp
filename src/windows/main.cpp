//
//  main.cpp
//  GameBoy
//
//  Created by Robin Carlsson on 2016-05-27.
//  Copyright © 2016 Robin Carlsson. All rights reserved.
//

#define SDL_MAIN_HANDLED

#include <Windows.h>
#include <ctime>
#include <random>
#include <iostream>
#include <SDL2\SDL.h>

#include "console.hpp"
#include "Window.hpp"

//int main(int argc, const char * argv[])
int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    //std::ios init(nullptr);
    //init.copyfmt(std::cout);
    //Cartridge rom("resources/tetris.gb");
    /*if(rom.loaded_successfully)
    {
        std::cout << "ROM size: " << rom.size << std::endl;
        std::cout << std::showbase << std::internal << std::setfill('0');
        for(int i = 0x134; i < 0x134 + 15; i++)
        {
            std::cout << std::setw(4) << std::hex << (char)rom.data[i] << std::endl;
        }
        
        for(int i = 0x134; i < 0x134 + 15; i++)
        {
            std::bitset<8> x(rom.data[i]);
            std::cout << x << std::endl;
        }
    }
    std::cout.copyfmt(init);*/
    
    /*CPU cpu;
    GPU gpu;
    MMU mmu;
    
    cpu_init(&cpu);
    mmu_init(&mmu);
    gpu_init(&gpu);
    
    cpu_init_defaults(&cpu, &mmu);
    
    for(int i = 0x100; i < 0x100 + 4; i++)
    {
        std::cout << std::setw(4) << std::hex << (int)rom.data[i] << std::endl;
    }
    
    while(1)
    {
        cpu_step(&cpu, &mmu, &rom);
        gpu_step(&gpu, &cpu, &mmu, &rom);
        cpu_interrupt_step(&cpu, &mmu, &rom);
    }*/
    
    srand(time(0));
    Window window;
    window_init(&window);
    
    Cartridge rom {"Resources\\tetris.gb"};

	if (!rom.loaded_successfully) 
	{
		std::cerr << "Rom failed to load" << std::endl;
		return 1;
	}

    Console console {&rom};
    int counter = 0;
    SDL_RenderClear(window.renderer);
    while(1) {
        console_step(&console);
        if((console.cpu.ticks % (4194304 / (60 * 24))) == 0)
        {
            window_update(&window, &console);
            window_draw(&window, &console);
        }
        
        if(counter > 0)
        {
            if(console.gpu.mode == GPU_MODE_OAM && console.gpu.ticks == 0)
            {
                //draw_sprites(&window, &console);
                //counter = 0;
            }
            //if(console.gpu.mode == GPU_MODE_OAM && console.gpu.scanline == 0)
            if(console.gpu.mode == GPU_MODE_VBLANK && console.gpu.ticks == 0 && console.gpu.scanline == 143)
            {
                //window_update(&window, &console);
                //if(counter % 100 == 0)
                //build_tile_buffer(&window, &console);
                
                //draw_bg(&window, &console);
                //window_draw(&window, &console);
                //SDL_RenderClear(window.renderer);
                //draw_bg(&window, &console);
                //draw_sprites(&window, &console);
                counter = 0;
            }
        }
        
        if(counter > 10000)
        {
            /*if(console.cpu.interrupt.master_enabled && (console.cpu.interrupt.flags & CPU_INTERRUPT_VBLANK) > 0)
            {
                window_update(&window, &console);
                window_draw(&window, &console);
                //counter = 0;
            }*/
            //build_tile_buffer(&window, &console);
            //window_update(&window, &console);
            //window_draw(&window, &console);
            //counter = 0;
        }
        counter++;
    }
    
    return 0;
}
