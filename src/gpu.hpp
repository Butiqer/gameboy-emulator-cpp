//
//  gpu.hpp
//  GameBoy
//
//  Created by Robin Carlsson on 2016-08-10.
//  Copyright © 2016 Robin Carlsson. All rights reserved.
//

#pragma once

#include <cstdint>
#include <stddef.h>

struct Cartridge;
struct MMU;
struct CPU;

enum gpu_mode
{
    GPU_MODE_HBLANK,
    GPU_MODE_VBLANK,
    GPU_MODE_OAM,
    GPU_MODE_VRAM
};

struct GPU
{
    uint8_t control;
    uint8_t scrollx;
    uint8_t scrolly;
    uint8_t scanline;
    gpu_mode mode;
    uint64_t ticks;
    uint64_t last_cpu_ticks;
};

void gpu_init(GPU*);
void gpu_step(GPU*, CPU*, MMU*, Cartridge*);
void gpu_render_graphics(GPU*, CPU*, MMU*, Cartridge*);
