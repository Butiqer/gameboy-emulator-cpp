//
//  Renderer.hpp
//  GameBoy
//
//  Created by Robin Carlsson on 2016-10-03.
//  Copyright © 2016 Robin Carlsson. All rights reserved.
//

#pragma once

#include <SDL2/SDL.h>
#include <SDL2_image/SDL_image.h>

#define TILE_BUFFER_WIDTH  256
#define TILE_BUFFER_HEIGHT 300

struct Console;

struct Window
{
    SDL_Window* window;
    SDL_Renderer* renderer;
    SDL_Texture* tile_buffer;
    int width;
    int height;
};

void window_init(Window*);
void window_update(Window*, Console*);
void window_update_touch(Window*, Console*);
void window_draw(Window*, Console*);
void window_set_fullscreen(Window*, bool);

void update_audio(Console*);

void build_tile_buffer(Window*, Console*);
void draw_bg(Window*, Console*);
void draw_sprites(Window*, Console*);
