//
//  opcodes.h
//  GameBoy
//
//  Created by Robin Carlsson on 2016-06-01.
//  Copyright © 2016 Robin Carlsson. All rights reserved.
//

#pragma once

#include <cstdint>

struct CPU;
struct MMU;
struct Cartridge;

enum instructions
{
    DAA,
    SCF,
    ADD,
    CCF,
    AND,
    SUB,
    RET,
    JR,
    DEC,
    NOP,
    RST,
    EI,
    DI,
    RRCA,
    CP,
    CALL,
    RLA,
    POP,
    PUSH,
    LDH,
    LD8,
    STOP,
    OR,
    CPL,
    INC,
    SBC,
    RLCA,
    HALT,
    RRA,
    ADC,
    PREFIX,
    XOR,
    JP,
    RETI,
    LD16
};

const uint8_t instruction_bytes[] = {
    1,   // NOP
    3,   // LD16
    1,   // LD8
    1,   // INC
    1,   // INC
    1,   // DEC
    2,   // LD8
    1,   // RLCA
    3,   // LD16
    1,   // ADD
    1,   // LD8
    1,   // DEC
    1,   // INC
    1,   // DEC
    2,   // LD8
    1,   // RRCA
    2,   // STOP
    3,   // LD16
    1,   // LD8
    1,   // INC
    1,   // INC
    1,   // DEC
    2,   // LD8
    1,   // RLA
    2,   // JR
    1,   // ADD
    1,   // LD8
    1,   // DEC
    1,   // INC
    1,   // DEC
    2,   // LD8
    1,   // RRA
    2,   // JR
    3,   // LD16
    1,   // LD8
    1,   // INC
    1,   // INC
    1,   // DEC
    2,   // LD8
    1,   // DAA
    2,   // JR
    1,   // ADD
    1,   // LD8
    1,   // DEC
    1,   // INC
    1,   // DEC
    2,   // LD8
    1,   // CPL
    2,   // JR
    3,   // LD16
    1,   // LD8
    1,   // INC
    1,   // INC
    1,   // DEC
    2,   // LD16
    1,   // SCF
    2,   // JR
    1,   // ADD
    1,   // LD8
    1,   // DEC
    1,   // INC
    1,   // DEC
    2,   // LD8
    1,   // CCF
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // HALT
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // LD8
    1,   // ADD
    1,   // ADD
    1,   // ADD
    1,   // ADD
    1,   // ADD
    1,   // ADD
    1,   // ADD
    1,   // ADD
    1,   // ADC
    1,   // ADC
    1,   // ADC
    1,   // ADC
    1,   // ADC
    1,   // ADC
    1,   // ADC
    1,   // ADC
    1,   // SUB
    1,   // SUB
    1,   // SUB
    1,   // SUB
    1,   // SUB
    1,   // SUB
    1,   // SUB
    1,   // SUB
    1,   // SBC
    1,   // SBC
    1,   // SBC
    1,   // SBC
    1,   // SBC
    1,   // SBC
    1,   // SBC
    1,   // SBC
    1,   // AND
    1,   // AND
    1,   // AND
    1,   // AND
    1,   // AND
    1,   // AND
    1,   // AND
    1,   // AND
    1,   // XOR
    1,   // XOR
    1,   // XOR
    1,   // XOR
    1,   // XOR
    1,   // XOR
    1,   // XOR
    1,   // XOR
    1,   // OR
    1,   // OR
    1,   // OR
    1,   // OR
    1,   // OR
    1,   // OR
    1,   // OR
    1,   // OR
    1,   // CP
    1,   // CP
    1,   // CP
    1,   // CP
    1,   // CP
    1,   // CP
    1,   // CP
    1,   // CP
    1,   // RET
    1,   // POP
    3,   // JP
    3,   // JP
    3,   // CALL
    1,   // PUSH
    2,   // ADD
    1,   // RST
    1,   // RET
    1,   // RET
    3,   // JP
    2,   // PREFIX
    3,   // CALL
    3,   // CALL
    2,   // ADC
    1,   // RST
    1,   // RET
    1,   // POP
    3,   // JP
    1,   // NOP
    3,   // CALL
    1,   // PUSH
    2,   // SUB
    1,   // RST
    1,   // RET
    1,   // RETI
    3,   // JP
    1,   // NOP
    3,   // CALL
    1,   // NOP
    2,   // SBC
    1,   // RST
    2,   // LDH
    1,   // POP
    1,   // LD8
    1,   // NOP
    1,   // NOP
    1,   // PUSH
    2,   // AND
    1,   // RST
    2,   // ADD
    1,   // JP
    3,   // LD8
    1,   // NOP
    1,   // NOP
    1,   // NOP
    2,   // XOR
    1,   // RST
    2,   // LDH
    1,   // POP
    2,   // LD8
    1,   // DI
    1,   // NOP
    1,   // PUSH
    2,   // OR
    1,   // RST
    2,   // LD16
    1,   // LD16
    3,   // LD8
    1,   // EI
    1,   // NOP
    1,   // NOP
    2,   // CP
    1,   // RST
};

const uint8_t instruction_type[] = {
    instructions::NOP,
    instructions::LD16,
    instructions::LD8,
    instructions::INC,
    instructions::INC,
    instructions::DEC,
    instructions::LD8,
    instructions::RLCA,
    instructions::LD16,
    instructions::ADD,
    instructions::LD8,
    instructions::DEC,
    instructions::INC,
    instructions::DEC,
    instructions::LD8,
    instructions::RRCA,
    instructions::STOP,
    instructions::LD16,
    instructions::LD8,
    instructions::INC,
    instructions::INC,
    instructions::DEC,
    instructions::LD8,
    instructions::RLA,
    instructions::JR,
    instructions::ADD,
    instructions::LD8,
    instructions::DEC,
    instructions::INC,
    instructions::DEC,
    instructions::LD8,
    instructions::RRA,
    instructions::JR,
    instructions::LD16,
    instructions::LD8,
    instructions::INC,
    instructions::INC,
    instructions::DEC,
    instructions::LD8,
    instructions::DAA,
    instructions::JR,
    instructions::ADD,
    instructions::LD8,
    instructions::DEC,
    instructions::INC,
    instructions::DEC,
    instructions::LD8,
    instructions::CPL,
    instructions::JR,
    instructions::LD16,
    instructions::LD8,
    instructions::INC,
    instructions::INC,
    instructions::DEC,
    instructions::LD16,
    instructions::SCF,
    instructions::JR,
    instructions::ADD,
    instructions::LD8,
    instructions::DEC,
    instructions::INC,
    instructions::DEC,
    instructions::LD8,
    instructions::CCF,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::HALT,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::LD8,
    instructions::ADD,
    instructions::ADD,
    instructions::ADD,
    instructions::ADD,
    instructions::ADD,
    instructions::ADD,
    instructions::ADD,
    instructions::ADD,
    instructions::ADC,
    instructions::ADC,
    instructions::ADC,
    instructions::ADC,
    instructions::ADC,
    instructions::ADC,
    instructions::ADC,
    instructions::ADC,
    instructions::SUB,
    instructions::SUB,
    instructions::SUB,
    instructions::SUB,
    instructions::SUB,
    instructions::SUB,
    instructions::SUB,
    instructions::SUB,
    instructions::SBC,
    instructions::SBC,
    instructions::SBC,
    instructions::SBC,
    instructions::SBC,
    instructions::SBC,
    instructions::SBC,
    instructions::SBC,
    instructions::AND,
    instructions::AND,
    instructions::AND,
    instructions::AND,
    instructions::AND,
    instructions::AND,
    instructions::AND,
    instructions::AND,
    instructions::XOR,
    instructions::XOR,
    instructions::XOR,
    instructions::XOR,
    instructions::XOR,
    instructions::XOR,
    instructions::XOR,
    instructions::XOR,
    instructions::OR,
    instructions::OR,
    instructions::OR,
    instructions::OR,
    instructions::OR,
    instructions::OR,
    instructions::OR,
    instructions::OR,
    instructions::CP,
    instructions::CP,
    instructions::CP,
    instructions::CP,
    instructions::CP,
    instructions::CP,
    instructions::CP,
    instructions::CP,
    instructions::RET,
    instructions::POP,
    instructions::JP,
    instructions::JP,
    instructions::CALL,
    instructions::PUSH,
    instructions::ADD,
    instructions::RST,
    instructions::RET,
    instructions::RET,
    instructions::JP,
    instructions::PREFIX,
    instructions::CALL,
    instructions::CALL,
    instructions::ADC,
    instructions::RST,
    instructions::RET,
    instructions::POP,
    instructions::JP,
    instructions::NOP,
    instructions::CALL,
    instructions::PUSH,
    instructions::SUB,
    instructions::RST,
    instructions::RET,
    instructions::RETI,
    instructions::JP,
    instructions::NOP,
    instructions::CALL,
    instructions::NOP,
    instructions::SBC,
    instructions::RST,
    instructions::LDH,
    instructions::POP,
    instructions::LD8,
    instructions::NOP,
    instructions::NOP,
    instructions::PUSH,
    instructions::AND,
    instructions::RST,
    instructions::ADD,
    instructions::JP,
    instructions::LD8,
    instructions::NOP,
    instructions::NOP,
    instructions::NOP,
    instructions::XOR,
    instructions::RST,
    instructions::LDH,
    instructions::POP,
    instructions::LD8,
    instructions::DI,
    instructions::NOP,
    instructions::PUSH,
    instructions::OR,
    instructions::RST,
    instructions::LD16,
    instructions::LD16,
    instructions::LD8,
    instructions::EI,
    instructions::NOP,
    instructions::NOP,
    instructions::CP,
    instructions::RST
};

const int first_operand[] = {
    0,  // None  - operand 1 for NOP
    1,  // BC    - operand 1 for LD16
    2,  // (BC)  - operand 1 for LD8
    1,  // BC    - operand 1 for INC
    3,  // B     - operand 1 for INC
    3,  // B     - operand 1 for DEC
    3,  // B     - operand 1 for LD8
    0,  // None  - operand 1 for RLCA
    4,  // (a16) - operand 1 for LD16
    5,  // HL    - operand 1 for ADD
    6,  // A     - operand 1 for LD8
    1,  // BC    - operand 1 for DEC
    7,  // C     - operand 1 for INC
    7,  // C     - operand 1 for DEC
    7,  // C     - operand 1 for LD8
    0,  // None  - operand 1 for RRCA
    8,  // 0     - operand 1 for STOP
    9,  // DE    - operand 1 for LD16
    10, // (DE)  - operand 1 for LD8
    9,  // DE    - operand 1 for INC
    11, // D     - operand 1 for INC
    11, // D     - operand 1 for DEC
    11, // D     - operand 1 for LD8
    0,  // None  - operand 1 for RLA
    12, // r8    - operand 1 for JR
    5,  // HL    - operand 1 for ADD
    6,  // A     - operand 1 for LD8
    9,  // DE    - operand 1 for DEC
    13, // E     - operand 1 for INC
    13, // E     - operand 1 for DEC
    13, // E     - operand 1 for LD8
    0,  // None  - operand 1 for RRA
    14, // NZ    - operand 1 for JR
    5,  // HL    - operand 1 for LD16
    15, // (HL+) - operand 1 for LD8
    5,  // HL    - operand 1 for INC
    16, // H     - operand 1 for INC
    16, // H     - operand 1 for DEC
    16, // H     - operand 1 for LD8
    0,  // None  - operand 1 for DAA
    17, // Z     - operand 1 for JR
    5,  // HL    - operand 1 for ADD
    6,  // A     - operand 1 for LD8
    5,  // HL    - operand 1 for DEC
    18, // L     - operand 1 for INC
    18, // L     - operand 1 for DEC
    18, // L     - operand 1 for LD8
    0,  // None  - operand 1 for CPL
    19, // NC    - operand 1 for JR
    20, // SP    - operand 1 for LD16
    21, // (HL-) - operand 1 for LD8
    20, // SP    - operand 1 for INC
    22, // (HL)  - operand 1 for INC
    22, // (HL)  - operand 1 for DEC
    22, // (HL)  - operand 1 for LD16
    0,  // None  - operand 1 for SCF
    7,  // C     - operand 1 for JR
    5,  // HL    - operand 1 for ADD
    6,  // A     - operand 1 for LD8
    20, // SP    - operand 1 for DEC
    6,  // A     - operand 1 for INC
    6,  // A     - operand 1 for DEC
    6,  // A     - operand 1 for LD8
    0,  // None  - operand 1 for CCF
    3,  // B     - operand 1 for LD8
    3,  // B     - operand 1 for LD8
    3,  // B     - operand 1 for LD8
    3,  // B     - operand 1 for LD8
    3,  // B     - operand 1 for LD8
    3,  // B     - operand 1 for LD8
    3,  // B     - operand 1 for LD8
    3,  // B     - operand 1 for LD8
    7,  // C     - operand 1 for LD8
    7,  // C     - operand 1 for LD8
    7,  // C     - operand 1 for LD8
    7,  // C     - operand 1 for LD8
    7,  // C     - operand 1 for LD8
    7,  // C     - operand 1 for LD8
    7,  // C     - operand 1 for LD8
    7,  // C     - operand 1 for LD8
    11, // D     - operand 1 for LD8
    11, // D     - operand 1 for LD8
    11, // D     - operand 1 for LD8
    11, // D     - operand 1 for LD8
    11, // D     - operand 1 for LD8
    11, // D     - operand 1 for LD8
    11, // D     - operand 1 for LD8
    11, // D     - operand 1 for LD8
    13, // E     - operand 1 for LD8
    13, // E     - operand 1 for LD8
    13, // E     - operand 1 for LD8
    13, // E     - operand 1 for LD8
    13, // E     - operand 1 for LD8
    13, // E     - operand 1 for LD8
    13, // E     - operand 1 for LD8
    13, // E     - operand 1 for LD8
    16, // H     - operand 1 for LD8
    16, // H     - operand 1 for LD8
    16, // H     - operand 1 for LD8
    16, // H     - operand 1 for LD8
    16, // H     - operand 1 for LD8
    16, // H     - operand 1 for LD8
    16, // H     - operand 1 for LD8
    16, // H     - operand 1 for LD8
    18, // L     - operand 1 for LD8
    18, // L     - operand 1 for LD8
    18, // L     - operand 1 for LD8
    18, // L     - operand 1 for LD8
    18, // L     - operand 1 for LD8
    18, // L     - operand 1 for LD8
    18, // L     - operand 1 for LD8
    18, // L     - operand 1 for LD8
    22, // (HL)  - operand 1 for LD8
    22, // (HL)  - operand 1 for LD8
    22, // (HL)  - operand 1 for LD8
    22, // (HL)  - operand 1 for LD8
    22, // (HL)  - operand 1 for LD8
    22, // (HL)  - operand 1 for LD8
    0,  // None  - operand 1 for HALT
    22, // (HL)  - operand 1 for LD8
    6,  // A     - operand 1 for LD8
    6,  // A     - operand 1 for LD8
    6,  // A     - operand 1 for LD8
    6,  // A     - operand 1 for LD8
    6,  // A     - operand 1 for LD8
    6,  // A     - operand 1 for LD8
    6,  // A     - operand 1 for LD8
    6,  // A     - operand 1 for LD8
    6,  // A     - operand 1 for ADD
    6,  // A     - operand 1 for ADD
    6,  // A     - operand 1 for ADD
    6,  // A     - operand 1 for ADD
    6,  // A     - operand 1 for ADD
    6,  // A     - operand 1 for ADD
    6,  // A     - operand 1 for ADD
    6,  // A     - operand 1 for ADD
    6,  // A     - operand 1 for ADC
    6,  // A     - operand 1 for ADC
    6,  // A     - operand 1 for ADC
    6,  // A     - operand 1 for ADC
    6,  // A     - operand 1 for ADC
    6,  // A     - operand 1 for ADC
    6,  // A     - operand 1 for ADC
    6,  // A     - operand 1 for ADC
    3,  // B     - operand 1 for SUB
    7,  // C     - operand 1 for SUB
    11, // D     - operand 1 for SUB
    13, // E     - operand 1 for SUB
    16, // H     - operand 1 for SUB
    18, // L     - operand 1 for SUB
    22, // (HL)  - operand 1 for SUB
    6,  // A     - operand 1 for SUB
    6,  // A     - operand 1 for SBC
    6,  // A     - operand 1 for SBC
    6,  // A     - operand 1 for SBC
    6,  // A     - operand 1 for SBC
    6,  // A     - operand 1 for SBC
    6,  // A     - operand 1 for SBC
    6,  // A     - operand 1 for SBC
    6,  // A     - operand 1 for SBC
    3,  // B     - operand 1 for AND
    7,  // C     - operand 1 for AND
    11, // D     - operand 1 for AND
    13, // E     - operand 1 for AND
    16, // H     - operand 1 for AND
    18, // L     - operand 1 for AND
    22, // (HL)  - operand 1 for AND
    6,  // A     - operand 1 for AND
    3,  // B     - operand 1 for XOR
    7,  // C     - operand 1 for XOR
    11, // D     - operand 1 for XOR
    13, // E     - operand 1 for XOR
    16, // H     - operand 1 for XOR
    18, // L     - operand 1 for XOR
    22, // (HL)  - operand 1 for XOR
    6,  // A     - operand 1 for XOR
    3,  // B     - operand 1 for OR
    7,  // C     - operand 1 for OR
    11, // D     - operand 1 for OR
    13, // E     - operand 1 for OR
    16, // H     - operand 1 for OR
    18, // L     - operand 1 for OR
    22, // (HL)  - operand 1 for OR
    6,  // A     - operand 1 for OR
    3,  // B     - operand 1 for CP
    7,  // C     - operand 1 for CP
    11, // D     - operand 1 for CP
    13, // E     - operand 1 for CP
    16, // H     - operand 1 for CP
    18, // L     - operand 1 for CP
    22, // (HL)  - operand 1 for CP
    6,  // A     - operand 1 for CP
    14, // NZ    - operand 1 for RET
    1,  // BC    - operand 1 for POP
    14, // NZ    - operand 1 for JP
    23, // a16   - operand 1 for JP
    14, // NZ    - operand 1 for CALL
    1,  // BC    - operand 1 for PUSH
    6,  // A     - operand 1 for ADD
    24, // 00H   - operand 1 for RST
    17, // Z     - operand 1 for RET
    0,  // None  - operand 1 for RET
    17, // Z     - operand 1 for JP
    25, // CB    - operand 1 for PREFIX
    17, // Z     - operand 1 for CALL
    23, // a16   - operand 1 for CALL
    6,  // A     - operand 1 for ADC
    26, // 08H   - operand 1 for RST
    19, // NC    - operand 1 for RET
    9,  // DE    - operand 1 for POP
    19, // NC    - operand 1 for JP
    19, // 0     - operand 1 for NOP
    19, // NC    - operand 1 for CALL
    9,  // DE    - operand 1 for PUSH
    27, // d8    - operand 1 for SUB
    28, // 10H   - operand 1 for RST
    7,  // C     - operand 1 for RET
    0,  // None  - operand 1 for RETI
    7,  // C     - operand 1 for JP
    7,  // 0     - operand 1 for NOP
    7,  // C     - operand 1 for CALL
    7,  // 0     - operand 1 for NOP
    6,  // A     - operand 1 for SBC
    29, // 18H   - operand 1 for RST
    30, // (a8)  - operand 1 for LDH
    5,  // HL    - operand 1 for POP
    31, // (C)   - operand 1 for LD8
    31, // 0     - operand 1 for NOP
    31, // 0     - operand 1 for NOP
    5,  // HL    - operand 1 for PUSH
    27, // d8    - operand 1 for AND
    32, // 20H   - operand 1 for RST
    20, // SP    - operand 1 for ADD
    22, // (HL)  - operand 1 for JP
    4,  // (a16) - operand 1 for LD8
    4,  // 0     - operand 1 for NOP
    4,  // 0     - operand 1 for NOP
    4,  // 0     - operand 1 for NOP
    27, // d8    - operand 1 for XOR
    33, // 28H   - operand 1 for RST
    6,  // A     - operand 1 for LDH
    34, // AF    - operand 1 for POP
    6,  // A     - operand 1 for LD8
    0,  // None  - operand 1 for DI
    0,  // 0     - operand 1 for NOP
    34, // AF    - operand 1 for PUSH
    27, // d8    - operand 1 for OR
    35, // 30H   - operand 1 for RST
    5,  // HL    - operand 1 for LD16
    20, // SP    - operand 1 for LD16
    6,  // A     - operand 1 for LD8
    0,  // None  - operand 1 for EI
    0,  // 0     - operand 1 for NOP
    0,  // 0     - operand 1 for NOP
    27, // d8    - operand 1 for CP
    36, // 38H   - operand 1 for RST
};

const int second_operand[] = {
    0,  // None  - operand 2 for NOP
    37, // d16   - operand 2 for LD16
    6,  // A     - operand 2 for LD8
    0,  // None  - operand 2 for INC
    0,  // None  - operand 2 for INC
    0,  // None  - operand 2 for DEC
    27, // d8    - operand 2 for LD8
    0,  // None  - operand 2 for RLCA
    20, // SP    - operand 2 for LD16
    1,  // BC    - operand 2 for ADD
    2,  // (BC)  - operand 2 for LD8
    0,  // None  - operand 2 for DEC
    0,  // None  - operand 2 for INC
    0,  // None  - operand 2 for DEC
    27, // d8    - operand 2 for LD8
    0,  // None  - operand 2 for RRCA
    0,  // None  - operand 2 for STOP
    37, // d16   - operand 2 for LD16
    6,  // A     - operand 2 for LD8
    0,  // None  - operand 2 for INC
    0,  // None  - operand 2 for INC
    0,  // None  - operand 2 for DEC
    27, // d8    - operand 2 for LD8
    0,  // None  - operand 2 for RLA
    0,  // None  - operand 2 for JR
    9,  // DE    - operand 2 for ADD
    10, // (DE)  - operand 2 for LD8
    0,  // None  - operand 2 for DEC
    0,  // None  - operand 2 for INC
    0,  // None  - operand 2 for DEC
    27, // d8    - operand 2 for LD8
    0,  // None  - operand 2 for RRA
    12, // r8    - operand 2 for JR
    37, // d16   - operand 2 for LD16
    6,  // A     - operand 2 for LD8
    0,  // None  - operand 2 for INC
    0,  // None  - operand 2 for INC
    0,  // None  - operand 2 for DEC
    27, // d8    - operand 2 for LD8
    0,  // None  - operand 2 for DAA
    12, // r8    - operand 2 for JR
    5,  // HL    - operand 2 for ADD
    15, // (HL+) - operand 2 for LD8
    0,  // None  - operand 2 for DEC
    0,  // None  - operand 2 for INC
    0,  // None  - operand 2 for DEC
    27, // d8    - operand 2 for LD8
    0,  // None  - operand 2 for CPL
    12, // r8    - operand 2 for JR
    37, // d16   - operand 2 for LD16
    6,  // A     - operand 2 for LD8
    0,  // None  - operand 2 for INC
    0,  // None  - operand 2 for INC
    0,  // None  - operand 2 for DEC
    27, // d8    - operand 2 for LD16
    0,  // None  - operand 2 for SCF
    12, // r8    - operand 2 for JR
    20, // SP    - operand 2 for ADD
    21, // (HL-) - operand 2 for LD8
    0,  // None  - operand 2 for DEC
    0,  // None  - operand 2 for INC
    0,  // None  - operand 2 for DEC
    27, // d8    - operand 2 for LD8
    0,  // None  - operand 2 for CCF
    3,  // B     - operand 2 for LD8
    7,  // C     - operand 2 for LD8
    11, // D     - operand 2 for LD8
    13, // E     - operand 2 for LD8
    16, // H     - operand 2 for LD8
    18, // L     - operand 2 for LD8
    22, // (HL)  - operand 2 for LD8
    6,  // A     - operand 2 for LD8
    3,  // B     - operand 2 for LD8
    7,  // C     - operand 2 for LD8
    11, // D     - operand 2 for LD8
    13, // E     - operand 2 for LD8
    16, // H     - operand 2 for LD8
    18, // L     - operand 2 for LD8
    22, // (HL)  - operand 2 for LD8
    6,  // A     - operand 2 for LD8
    3,  // B     - operand 2 for LD8
    7,  // C     - operand 2 for LD8
    11, // D     - operand 2 for LD8
    13, // E     - operand 2 for LD8
    16, // H     - operand 2 for LD8
    18, // L     - operand 2 for LD8
    22, // (HL)  - operand 2 for LD8
    6,  // A     - operand 2 for LD8
    3,  // B     - operand 2 for LD8
    7,  // C     - operand 2 for LD8
    11, // D     - operand 2 for LD8
    13, // E     - operand 2 for LD8
    16, // H     - operand 2 for LD8
    18, // L     - operand 2 for LD8
    22, // (HL)  - operand 2 for LD8
    6,  // A     - operand 2 for LD8
    3,  // B     - operand 2 for LD8
    7,  // C     - operand 2 for LD8
    11, // D     - operand 2 for LD8
    13, // E     - operand 2 for LD8
    16, // H     - operand 2 for LD8
    18, // L     - operand 2 for LD8
    22, // (HL)  - operand 2 for LD8
    6,  // A     - operand 2 for LD8
    3,  // B     - operand 2 for LD8
    7,  // C     - operand 2 for LD8
    11, // D     - operand 2 for LD8
    13, // E     - operand 2 for LD8
    16, // H     - operand 2 for LD8
    18, // L     - operand 2 for LD8
    22, // (HL)  - operand 2 for LD8
    6,  // A     - operand 2 for LD8
    3,  // B     - operand 2 for LD8
    7,  // C     - operand 2 for LD8
    11, // D     - operand 2 for LD8
    13, // E     - operand 2 for LD8
    16, // H     - operand 2 for LD8
    18, // L     - operand 2 for LD8
    0,  // None  - operand 2 for HALT
    6,  // A     - operand 2 for LD8
    3,  // B     - operand 2 for LD8
    7,  // C     - operand 2 for LD8
    11, // D     - operand 2 for LD8
    13, // E     - operand 2 for LD8
    16, // H     - operand 2 for LD8
    18, // L     - operand 2 for LD8
    22, // (HL)  - operand 2 for LD8
    6,  // A     - operand 2 for LD8
    3,  // B     - operand 2 for ADD
    7,  // C     - operand 2 for ADD
    11, // D     - operand 2 for ADD
    13, // E     - operand 2 for ADD
    16, // H     - operand 2 for ADD
    18, // L     - operand 2 for ADD
    22, // (HL)  - operand 2 for ADD
    6,  // A     - operand 2 for ADD
    3,  // B     - operand 2 for ADC
    7,  // C     - operand 2 for ADC
    11, // D     - operand 2 for ADC
    13, // E     - operand 2 for ADC
    16, // H     - operand 2 for ADC
    18, // L     - operand 2 for ADC
    22, // (HL)  - operand 2 for ADC
    6,  // A     - operand 2 for ADC
    0,  // None  - operand 2 for SUB
    0,  // None  - operand 2 for SUB
    0,  // None  - operand 2 for SUB
    0,  // None  - operand 2 for SUB
    0,  // None  - operand 2 for SUB
    0,  // None  - operand 2 for SUB
    0,  // None  - operand 2 for SUB
    0,  // None  - operand 2 for SUB
    3,  // B     - operand 2 for SBC
    7,  // C     - operand 2 for SBC
    11, // D     - operand 2 for SBC
    13, // E     - operand 2 for SBC
    16, // H     - operand 2 for SBC
    18, // L     - operand 2 for SBC
    22, // (HL)  - operand 2 for SBC
    6,  // A     - operand 2 for SBC
    0,  // None  - operand 2 for AND
    0,  // None  - operand 2 for AND
    0,  // None  - operand 2 for AND
    0,  // None  - operand 2 for AND
    0,  // None  - operand 2 for AND
    0,  // None  - operand 2 for AND
    0,  // None  - operand 2 for AND
    0,  // None  - operand 2 for AND
    0,  // None  - operand 2 for XOR
    0,  // None  - operand 2 for XOR
    0,  // None  - operand 2 for XOR
    0,  // None  - operand 2 for XOR
    0,  // None  - operand 2 for XOR
    0,  // None  - operand 2 for XOR
    0,  // None  - operand 2 for XOR
    0,  // None  - operand 2 for XOR
    0,  // None  - operand 2 for OR
    0,  // None  - operand 2 for OR
    0,  // None  - operand 2 for OR
    0,  // None  - operand 2 for OR
    0,  // None  - operand 2 for OR
    0,  // None  - operand 2 for OR
    0,  // None  - operand 2 for OR
    0,  // None  - operand 2 for OR
    0,  // None  - operand 2 for CP
    0,  // None  - operand 2 for CP
    0,  // None  - operand 2 for CP
    0,  // None  - operand 2 for CP
    0,  // None  - operand 2 for CP
    0,  // None  - operand 2 for CP
    0,  // None  - operand 2 for CP
    0,  // None  - operand 2 for CP
    0,  // None  - operand 2 for RET
    0,  // None  - operand 2 for POP
    23, // a16   - operand 2 for JP
    0,  // None  - operand 2 for JP
    23, // a16   - operand 2 for CALL
    0,  // None  - operand 2 for PUSH
    27, // d8    - operand 2 for ADD
    0,  // None  - operand 2 for RST
    0,  // None  - operand 2 for RET
    0,  // None  - operand 2 for RET
    23, // a16   - operand 2 for JP
    0,  // None  - operand 2 for PREFIX
    23, // a16   - operand 2 for CALL
    0,  // None  - operand 2 for CALL
    27, // d8    - operand 2 for ADC
    0,  // None  - operand 2 for RST
    0,  // None  - operand 2 for RET
    0,  // None  - operand 2 for POP
    23, // a16   - operand 2 for JP
    23, // 0     - operand 2 for NOP
    23, // a16   - operand 2 for CALL
    0,  // None  - operand 2 for PUSH
    0,  // None  - operand 2 for SUB
    0,  // None  - operand 2 for RST
    0,  // None  - operand 2 for RET
    0,  // None  - operand 2 for RETI
    23, // a16   - operand 2 for JP
    23, // 0     - operand 2 for NOP
    23, // a16   - operand 2 for CALL
    23, // 0     - operand 2 for NOP
    27, // d8    - operand 2 for SBC
    0,  // None  - operand 2 for RST
    6,  // A     - operand 2 for LDH
    0,  // None  - operand 2 for POP
    6,  // A     - operand 2 for LD8
    6,  // 0     - operand 2 for NOP
    6,  // 0     - operand 2 for NOP
    0,  // None  - operand 2 for PUSH
    0,  // None  - operand 2 for AND
    0,  // None  - operand 2 for RST
    12, // r8    - operand 2 for ADD
    0,  // None  - operand 2 for JP
    6,  // A     - operand 2 for LD8
    6,  // 0     - operand 2 for NOP
    6,  // 0     - operand 2 for NOP
    6,  // 0     - operand 2 for NOP
    0,  // None  - operand 2 for XOR
    0,  // None  - operand 2 for RST
    30, // (a8)  - operand 2 for LDH
    0,  // None  - operand 2 for POP
    31, // (C)   - operand 2 for LD8
    0,  // None  - operand 2 for DI
    0,  // 0     - operand 2 for NOP
    0,  // None  - operand 2 for PUSH
    0,  // None  - operand 2 for OR
    0,  // None  - operand 2 for RST
    38, // SP+r8 - operand 2 for LD16
    5,  // HL    - operand 2 for LD16
    4,  // (a16) - operand 2 for LD8
    0,  // None  - operand 2 for EI
    0,  // 0     - operand 2 for NOP
    0,  // 0     - operand 2 for NOP
    0,  // None  - operand 2 for CP
    0   // None  - operand 2 for RST
};

enum operand_type {
    register_value,
    register_address,
    const_value,
    const_address,
    flag,
    zero,
    SP,
    cbprefix,
    reset,
    sp_r8
};

const operand_type operand_types[] = {
    operand_type::register_value,    // None
    operand_type::register_value,    // BC
    operand_type::register_address,  // (BC)
    operand_type::register_value,    // B
    operand_type::const_address,     // (a16)
    operand_type::register_value,    // HL
    operand_type::register_value,    // A
    operand_type::register_value,    // C
    operand_type::zero,              // 0
    operand_type::register_value,    // DE
    operand_type::register_address,  // (DE)
    operand_type::register_value,    // D
    operand_type::const_value,       // r8
    operand_type::register_value,    // E
    operand_type::flag,              // NZ
    operand_type::register_address,  // (HL+)
    operand_type::register_value,    // H
    operand_type::flag,              // Z
    operand_type::register_value,    // L
    operand_type::flag,              // NC
    operand_type::SP,                // SP
    operand_type::register_address,  // (HL-)
    operand_type::register_address,  // (HL)
    operand_type::const_value,       // a16
    operand_type::reset,             // 00H
    operand_type::cbprefix,          // CB
    operand_type::reset,             // 08H
    operand_type::const_value,       // d8
    operand_type::reset,             // 10H
    operand_type::reset,             // 18H
    operand_type::const_address,     // (a8)
    operand_type::register_address,  // (C)
    operand_type::reset,             // 20H
    operand_type::reset,             // 28H
    operand_type::register_value,    // AF
    operand_type::reset,             // 30H
    operand_type::reset,             // 38H
    operand_type::const_value,       // d16
    operand_type::sp_r8,             // SP+r8
};

void opcode_0x0(CPU*, MMU*, Cartridge*);
void opcode_0x1(CPU*, MMU*, Cartridge*);
void opcode_0x2(CPU*, MMU*, Cartridge*);
void opcode_0x3(CPU*, MMU*, Cartridge*);
void opcode_0x4(CPU*, MMU*, Cartridge*);
void opcode_0x5(CPU*, MMU*, Cartridge*);
void opcode_0x6(CPU*, MMU*, Cartridge*);
void opcode_0x7(CPU*, MMU*, Cartridge*);
void opcode_0x8(CPU*, MMU*, Cartridge*);
void opcode_0x9(CPU*, MMU*, Cartridge*);
void opcode_0xa(CPU*, MMU*, Cartridge*);
void opcode_0xb(CPU*, MMU*, Cartridge*);
void opcode_0xc(CPU*, MMU*, Cartridge*);
void opcode_0xd(CPU*, MMU*, Cartridge*);
void opcode_0xe(CPU*, MMU*, Cartridge*);
void opcode_0xf(CPU*, MMU*, Cartridge*);
void opcode_0x10(CPU*, MMU*, Cartridge*);
void opcode_0x11(CPU*, MMU*, Cartridge*);
void opcode_0x12(CPU*, MMU*, Cartridge*);
void opcode_0x13(CPU*, MMU*, Cartridge*);
void opcode_0x14(CPU*, MMU*, Cartridge*);
void opcode_0x15(CPU*, MMU*, Cartridge*);
void opcode_0x16(CPU*, MMU*, Cartridge*);
void opcode_0x17(CPU*, MMU*, Cartridge*);
void opcode_0x18(CPU*, MMU*, Cartridge*);
void opcode_0x19(CPU*, MMU*, Cartridge*);
void opcode_0x1a(CPU*, MMU*, Cartridge*);
void opcode_0x1b(CPU*, MMU*, Cartridge*);
void opcode_0x1c(CPU*, MMU*, Cartridge*);
void opcode_0x1d(CPU*, MMU*, Cartridge*);
void opcode_0x1e(CPU*, MMU*, Cartridge*);
void opcode_0x1f(CPU*, MMU*, Cartridge*);
void opcode_0x20(CPU*, MMU*, Cartridge*);
void opcode_0x21(CPU*, MMU*, Cartridge*);
void opcode_0x22(CPU*, MMU*, Cartridge*);
void opcode_0x23(CPU*, MMU*, Cartridge*);
void opcode_0x24(CPU*, MMU*, Cartridge*);
void opcode_0x25(CPU*, MMU*, Cartridge*);
void opcode_0x26(CPU*, MMU*, Cartridge*);
void opcode_0x27(CPU*, MMU*, Cartridge*);
void opcode_0x28(CPU*, MMU*, Cartridge*);
void opcode_0x29(CPU*, MMU*, Cartridge*);
void opcode_0x2a(CPU*, MMU*, Cartridge*);
void opcode_0x2b(CPU*, MMU*, Cartridge*);
void opcode_0x2c(CPU*, MMU*, Cartridge*);
void opcode_0x2d(CPU*, MMU*, Cartridge*);
void opcode_0x2e(CPU*, MMU*, Cartridge*);
void opcode_0x2f(CPU*, MMU*, Cartridge*);
void opcode_0x30(CPU*, MMU*, Cartridge*);
void opcode_0x31(CPU*, MMU*, Cartridge*);
void opcode_0x32(CPU*, MMU*, Cartridge*);
void opcode_0x33(CPU*, MMU*, Cartridge*);
void opcode_0x34(CPU*, MMU*, Cartridge*);
void opcode_0x35(CPU*, MMU*, Cartridge*);
void opcode_0x36(CPU*, MMU*, Cartridge*);
void opcode_0x37(CPU*, MMU*, Cartridge*);
void opcode_0x38(CPU*, MMU*, Cartridge*);
void opcode_0x39(CPU*, MMU*, Cartridge*);
void opcode_0x3a(CPU*, MMU*, Cartridge*);
void opcode_0x3b(CPU*, MMU*, Cartridge*);
void opcode_0x3c(CPU*, MMU*, Cartridge*);
void opcode_0x3d(CPU*, MMU*, Cartridge*);
void opcode_0x3e(CPU*, MMU*, Cartridge*);
void opcode_0x3f(CPU*, MMU*, Cartridge*);
void opcode_0x40(CPU*, MMU*, Cartridge*);
void opcode_0x41(CPU*, MMU*, Cartridge*);
void opcode_0x42(CPU*, MMU*, Cartridge*);
void opcode_0x43(CPU*, MMU*, Cartridge*);
void opcode_0x44(CPU*, MMU*, Cartridge*);
void opcode_0x45(CPU*, MMU*, Cartridge*);
void opcode_0x46(CPU*, MMU*, Cartridge*);
void opcode_0x47(CPU*, MMU*, Cartridge*);
void opcode_0x48(CPU*, MMU*, Cartridge*);
void opcode_0x49(CPU*, MMU*, Cartridge*);
void opcode_0x4a(CPU*, MMU*, Cartridge*);
void opcode_0x4b(CPU*, MMU*, Cartridge*);
void opcode_0x4c(CPU*, MMU*, Cartridge*);
void opcode_0x4d(CPU*, MMU*, Cartridge*);
void opcode_0x4e(CPU*, MMU*, Cartridge*);
void opcode_0x4f(CPU*, MMU*, Cartridge*);
void opcode_0x50(CPU*, MMU*, Cartridge*);
void opcode_0x51(CPU*, MMU*, Cartridge*);
void opcode_0x52(CPU*, MMU*, Cartridge*);
void opcode_0x53(CPU*, MMU*, Cartridge*);
void opcode_0x54(CPU*, MMU*, Cartridge*);
void opcode_0x55(CPU*, MMU*, Cartridge*);
void opcode_0x56(CPU*, MMU*, Cartridge*);
void opcode_0x57(CPU*, MMU*, Cartridge*);
void opcode_0x58(CPU*, MMU*, Cartridge*);
void opcode_0x59(CPU*, MMU*, Cartridge*);
void opcode_0x5a(CPU*, MMU*, Cartridge*);
void opcode_0x5b(CPU*, MMU*, Cartridge*);
void opcode_0x5c(CPU*, MMU*, Cartridge*);
void opcode_0x5d(CPU*, MMU*, Cartridge*);
void opcode_0x5e(CPU*, MMU*, Cartridge*);
void opcode_0x5f(CPU*, MMU*, Cartridge*);
void opcode_0x60(CPU*, MMU*, Cartridge*);
void opcode_0x61(CPU*, MMU*, Cartridge*);
void opcode_0x62(CPU*, MMU*, Cartridge*);
void opcode_0x63(CPU*, MMU*, Cartridge*);
void opcode_0x64(CPU*, MMU*, Cartridge*);
void opcode_0x65(CPU*, MMU*, Cartridge*);
void opcode_0x66(CPU*, MMU*, Cartridge*);
void opcode_0x67(CPU*, MMU*, Cartridge*);
void opcode_0x68(CPU*, MMU*, Cartridge*);
void opcode_0x69(CPU*, MMU*, Cartridge*);
void opcode_0x6a(CPU*, MMU*, Cartridge*);
void opcode_0x6b(CPU*, MMU*, Cartridge*);
void opcode_0x6c(CPU*, MMU*, Cartridge*);
void opcode_0x6d(CPU*, MMU*, Cartridge*);
void opcode_0x6e(CPU*, MMU*, Cartridge*);
void opcode_0x6f(CPU*, MMU*, Cartridge*);
void opcode_0x70(CPU*, MMU*, Cartridge*);
void opcode_0x71(CPU*, MMU*, Cartridge*);
void opcode_0x72(CPU*, MMU*, Cartridge*);
void opcode_0x73(CPU*, MMU*, Cartridge*);
void opcode_0x74(CPU*, MMU*, Cartridge*);
void opcode_0x75(CPU*, MMU*, Cartridge*);
void opcode_0x76(CPU*, MMU*, Cartridge*);
void opcode_0x77(CPU*, MMU*, Cartridge*);
void opcode_0x78(CPU*, MMU*, Cartridge*);
void opcode_0x79(CPU*, MMU*, Cartridge*);
void opcode_0x7a(CPU*, MMU*, Cartridge*);
void opcode_0x7b(CPU*, MMU*, Cartridge*);
void opcode_0x7c(CPU*, MMU*, Cartridge*);
void opcode_0x7d(CPU*, MMU*, Cartridge*);
void opcode_0x7e(CPU*, MMU*, Cartridge*);
void opcode_0x7f(CPU*, MMU*, Cartridge*);
void opcode_0x80(CPU*, MMU*, Cartridge*);
void opcode_0x81(CPU*, MMU*, Cartridge*);
void opcode_0x82(CPU*, MMU*, Cartridge*);
void opcode_0x83(CPU*, MMU*, Cartridge*);
void opcode_0x84(CPU*, MMU*, Cartridge*);
void opcode_0x85(CPU*, MMU*, Cartridge*);
void opcode_0x86(CPU*, MMU*, Cartridge*);
void opcode_0x87(CPU*, MMU*, Cartridge*);
void opcode_0x88(CPU*, MMU*, Cartridge*);
void opcode_0x89(CPU*, MMU*, Cartridge*);
void opcode_0x8a(CPU*, MMU*, Cartridge*);
void opcode_0x8b(CPU*, MMU*, Cartridge*);
void opcode_0x8c(CPU*, MMU*, Cartridge*);
void opcode_0x8d(CPU*, MMU*, Cartridge*);
void opcode_0x8e(CPU*, MMU*, Cartridge*);
void opcode_0x8f(CPU*, MMU*, Cartridge*);
void opcode_0x90(CPU*, MMU*, Cartridge*);
void opcode_0x91(CPU*, MMU*, Cartridge*);
void opcode_0x92(CPU*, MMU*, Cartridge*);
void opcode_0x93(CPU*, MMU*, Cartridge*);
void opcode_0x94(CPU*, MMU*, Cartridge*);
void opcode_0x95(CPU*, MMU*, Cartridge*);
void opcode_0x96(CPU*, MMU*, Cartridge*);
void opcode_0x97(CPU*, MMU*, Cartridge*);
void opcode_0x98(CPU*, MMU*, Cartridge*);
void opcode_0x99(CPU*, MMU*, Cartridge*);
void opcode_0x9a(CPU*, MMU*, Cartridge*);
void opcode_0x9b(CPU*, MMU*, Cartridge*);
void opcode_0x9c(CPU*, MMU*, Cartridge*);
void opcode_0x9d(CPU*, MMU*, Cartridge*);
void opcode_0x9e(CPU*, MMU*, Cartridge*);
void opcode_0x9f(CPU*, MMU*, Cartridge*);
void opcode_0xa0(CPU*, MMU*, Cartridge*);
void opcode_0xa1(CPU*, MMU*, Cartridge*);
void opcode_0xa2(CPU*, MMU*, Cartridge*);
void opcode_0xa3(CPU*, MMU*, Cartridge*);
void opcode_0xa4(CPU*, MMU*, Cartridge*);
void opcode_0xa5(CPU*, MMU*, Cartridge*);
void opcode_0xa6(CPU*, MMU*, Cartridge*);
void opcode_0xa7(CPU*, MMU*, Cartridge*);
void opcode_0xa8(CPU*, MMU*, Cartridge*);
void opcode_0xa9(CPU*, MMU*, Cartridge*);
void opcode_0xaa(CPU*, MMU*, Cartridge*);
void opcode_0xab(CPU*, MMU*, Cartridge*);
void opcode_0xac(CPU*, MMU*, Cartridge*);
void opcode_0xad(CPU*, MMU*, Cartridge*);
void opcode_0xae(CPU*, MMU*, Cartridge*);
void opcode_0xaf(CPU*, MMU*, Cartridge*);
void opcode_0xb0(CPU*, MMU*, Cartridge*);
void opcode_0xb1(CPU*, MMU*, Cartridge*);
void opcode_0xb2(CPU*, MMU*, Cartridge*);
void opcode_0xb3(CPU*, MMU*, Cartridge*);
void opcode_0xb4(CPU*, MMU*, Cartridge*);
void opcode_0xb5(CPU*, MMU*, Cartridge*);
void opcode_0xb6(CPU*, MMU*, Cartridge*);
void opcode_0xb7(CPU*, MMU*, Cartridge*);
void opcode_0xb8(CPU*, MMU*, Cartridge*);
void opcode_0xb9(CPU*, MMU*, Cartridge*);
void opcode_0xba(CPU*, MMU*, Cartridge*);
void opcode_0xbb(CPU*, MMU*, Cartridge*);
void opcode_0xbc(CPU*, MMU*, Cartridge*);
void opcode_0xbd(CPU*, MMU*, Cartridge*);
void opcode_0xbe(CPU*, MMU*, Cartridge*);
void opcode_0xbf(CPU*, MMU*, Cartridge*);
void opcode_0xc0(CPU*, MMU*, Cartridge*);
void opcode_0xc1(CPU*, MMU*, Cartridge*);
void opcode_0xc2(CPU*, MMU*, Cartridge*);
void opcode_0xc3(CPU*, MMU*, Cartridge*);
void opcode_0xc4(CPU*, MMU*, Cartridge*);
void opcode_0xc5(CPU*, MMU*, Cartridge*);
void opcode_0xc6(CPU*, MMU*, Cartridge*);
void opcode_0xc7(CPU*, MMU*, Cartridge*);
void opcode_0xc8(CPU*, MMU*, Cartridge*);
void opcode_0xc9(CPU*, MMU*, Cartridge*);
void opcode_0xca(CPU*, MMU*, Cartridge*);
void opcode_0xcb(CPU*, MMU*, Cartridge*);
void opcode_0xcc(CPU*, MMU*, Cartridge*);
void opcode_0xcd(CPU*, MMU*, Cartridge*);
void opcode_0xce(CPU*, MMU*, Cartridge*);
void opcode_0xcf(CPU*, MMU*, Cartridge*);
void opcode_0xd0(CPU*, MMU*, Cartridge*);
void opcode_0xd1(CPU*, MMU*, Cartridge*);
void opcode_0xd2(CPU*, MMU*, Cartridge*);
void opcode_0xd3(CPU*, MMU*, Cartridge*);
void opcode_0xd4(CPU*, MMU*, Cartridge*);
void opcode_0xd5(CPU*, MMU*, Cartridge*);
void opcode_0xd6(CPU*, MMU*, Cartridge*);
void opcode_0xd7(CPU*, MMU*, Cartridge*);
void opcode_0xd8(CPU*, MMU*, Cartridge*);
void opcode_0xd9(CPU*, MMU*, Cartridge*);
void opcode_0xda(CPU*, MMU*, Cartridge*);
void opcode_0xdb(CPU*, MMU*, Cartridge*);
void opcode_0xdc(CPU*, MMU*, Cartridge*);
void opcode_0xdd(CPU*, MMU*, Cartridge*);
void opcode_0xde(CPU*, MMU*, Cartridge*);
void opcode_0xdf(CPU*, MMU*, Cartridge*);
void opcode_0xe0(CPU*, MMU*, Cartridge*);
void opcode_0xe1(CPU*, MMU*, Cartridge*);
void opcode_0xe2(CPU*, MMU*, Cartridge*);
void opcode_0xe3(CPU*, MMU*, Cartridge*);
void opcode_0xe4(CPU*, MMU*, Cartridge*);
void opcode_0xe5(CPU*, MMU*, Cartridge*);
void opcode_0xe6(CPU*, MMU*, Cartridge*);
void opcode_0xe7(CPU*, MMU*, Cartridge*);
void opcode_0xe8(CPU*, MMU*, Cartridge*);
void opcode_0xe9(CPU*, MMU*, Cartridge*);
void opcode_0xea(CPU*, MMU*, Cartridge*);
void opcode_0xeb(CPU*, MMU*, Cartridge*);
void opcode_0xec(CPU*, MMU*, Cartridge*);
void opcode_0xed(CPU*, MMU*, Cartridge*);
void opcode_0xee(CPU*, MMU*, Cartridge*);
void opcode_0xef(CPU*, MMU*, Cartridge*);
void opcode_0xf0(CPU*, MMU*, Cartridge*);
void opcode_0xf1(CPU*, MMU*, Cartridge*);
void opcode_0xf2(CPU*, MMU*, Cartridge*);
void opcode_0xf3(CPU*, MMU*, Cartridge*);
void opcode_0xf4(CPU*, MMU*, Cartridge*);
void opcode_0xf5(CPU*, MMU*, Cartridge*);
void opcode_0xf6(CPU*, MMU*, Cartridge*);
void opcode_0xf7(CPU*, MMU*, Cartridge*);
void opcode_0xf8(CPU*, MMU*, Cartridge*);
void opcode_0xf9(CPU*, MMU*, Cartridge*);
void opcode_0xfa(CPU*, MMU*, Cartridge*);
void opcode_0xfb(CPU*, MMU*, Cartridge*);
void opcode_0xfc(CPU*, MMU*, Cartridge*);
void opcode_0xfd(CPU*, MMU*, Cartridge*);
void opcode_0xfe(CPU*, MMU*, Cartridge*);
void opcode_0xff(CPU*, MMU*, Cartridge*);

typedef void (*opcode_func)(CPU*, MMU*, Cartridge*);
const opcode_func opcode_funcs[] = {
    &opcode_0x0,
    &opcode_0x1,
    &opcode_0x2,
    &opcode_0x3,
    &opcode_0x4,
    &opcode_0x5,
    &opcode_0x6,
    &opcode_0x7,
    &opcode_0x8,
    &opcode_0x9,
    &opcode_0xa,
    &opcode_0xb,
    &opcode_0xc,
    &opcode_0xd,
    &opcode_0xe,
    &opcode_0xf,
    &opcode_0x10,
    &opcode_0x11,
    &opcode_0x12,
    &opcode_0x13,
    &opcode_0x14,
    &opcode_0x15,
    &opcode_0x16,
    &opcode_0x17,
    &opcode_0x18,
    &opcode_0x19,
    &opcode_0x1a,
    &opcode_0x1b,
    &opcode_0x1c,
    &opcode_0x1d,
    &opcode_0x1e,
    &opcode_0x1f,
    &opcode_0x20,
    &opcode_0x21,
    &opcode_0x22,
    &opcode_0x23,
    &opcode_0x24,
    &opcode_0x25,
    &opcode_0x26,
    &opcode_0x27,
    &opcode_0x28,
    &opcode_0x29,
    &opcode_0x2a,
    &opcode_0x2b,
    &opcode_0x2c,
    &opcode_0x2d,
    &opcode_0x2e,
    &opcode_0x2f,
    &opcode_0x30,
    &opcode_0x31,
    &opcode_0x32,
    &opcode_0x33,
    &opcode_0x34,
    &opcode_0x35,
    &opcode_0x36,
    &opcode_0x37,
    &opcode_0x38,
    &opcode_0x39,
    &opcode_0x3a,
    &opcode_0x3b,
    &opcode_0x3c,
    &opcode_0x3d,
    &opcode_0x3e,
    &opcode_0x3f,
    &opcode_0x40,
    &opcode_0x41,
    &opcode_0x42,
    &opcode_0x43,
    &opcode_0x44,
    &opcode_0x45,
    &opcode_0x46,
    &opcode_0x47,
    &opcode_0x48,
    &opcode_0x49,
    &opcode_0x4a,
    &opcode_0x4b,
    &opcode_0x4c,
    &opcode_0x4d,
    &opcode_0x4e,
    &opcode_0x4f,
    &opcode_0x50,
    &opcode_0x51,
    &opcode_0x52,
    &opcode_0x53,
    &opcode_0x54,
    &opcode_0x55,
    &opcode_0x56,
    &opcode_0x57,
    &opcode_0x58,
    &opcode_0x59,
    &opcode_0x5a,
    &opcode_0x5b,
    &opcode_0x5c,
    &opcode_0x5d,
    &opcode_0x5e,
    &opcode_0x5f,
    &opcode_0x60,
    &opcode_0x61,
    &opcode_0x62,
    &opcode_0x63,
    &opcode_0x64,
    &opcode_0x65,
    &opcode_0x66,
    &opcode_0x67,
    &opcode_0x68,
    &opcode_0x69,
    &opcode_0x6a,
    &opcode_0x6b,
    &opcode_0x6c,
    &opcode_0x6d,
    &opcode_0x6e,
    &opcode_0x6f,
    &opcode_0x70,
    &opcode_0x71,
    &opcode_0x72,
    &opcode_0x73,
    &opcode_0x74,
    &opcode_0x75,
    &opcode_0x76,
    &opcode_0x77,
    &opcode_0x78,
    &opcode_0x79,
    &opcode_0x7a,
    &opcode_0x7b,
    &opcode_0x7c,
    &opcode_0x7d,
    &opcode_0x7e,
    &opcode_0x7f,
    &opcode_0x80,
    &opcode_0x81,
    &opcode_0x82,
    &opcode_0x83,
    &opcode_0x84,
    &opcode_0x85,
    &opcode_0x86,
    &opcode_0x87,
    &opcode_0x88,
    &opcode_0x89,
    &opcode_0x8a,
    &opcode_0x8b,
    &opcode_0x8c,
    &opcode_0x8d,
    &opcode_0x8e,
    &opcode_0x8f,
    &opcode_0x90,
    &opcode_0x91,
    &opcode_0x92,
    &opcode_0x93,
    &opcode_0x94,
    &opcode_0x95,
    &opcode_0x96,
    &opcode_0x97,
    &opcode_0x98,
    &opcode_0x99,
    &opcode_0x9a,
    &opcode_0x9b,
    &opcode_0x9c,
    &opcode_0x9d,
    &opcode_0x9e,
    &opcode_0x9f,
    &opcode_0xa0,
    &opcode_0xa1,
    &opcode_0xa2,
    &opcode_0xa3,
    &opcode_0xa4,
    &opcode_0xa5,
    &opcode_0xa6,
    &opcode_0xa7,
    &opcode_0xa8,
    &opcode_0xa9,
    &opcode_0xaa,
    &opcode_0xab,
    &opcode_0xac,
    &opcode_0xad,
    &opcode_0xae,
    &opcode_0xaf,
    &opcode_0xb0,
    &opcode_0xb1,
    &opcode_0xb2,
    &opcode_0xb3,
    &opcode_0xb4,
    &opcode_0xb5,
    &opcode_0xb6,
    &opcode_0xb7,
    &opcode_0xb8,
    &opcode_0xb9,
    &opcode_0xba,
    &opcode_0xbb,
    &opcode_0xbc,
    &opcode_0xbd,
    &opcode_0xbe,
    &opcode_0xbf,
    &opcode_0xc0,
    &opcode_0xc1,
    &opcode_0xc2,
    &opcode_0xc3,
    &opcode_0xc4,
    &opcode_0xc5,
    &opcode_0xc6,
    &opcode_0xc7,
    &opcode_0xc8,
    &opcode_0xc9,
    &opcode_0xca,
    &opcode_0xcb,
    &opcode_0xcc,
    &opcode_0xcd,
    &opcode_0xce,
    &opcode_0xcf,
    &opcode_0xd0,
    &opcode_0xd1,
    &opcode_0xd2,
    &opcode_0x0,
    &opcode_0xd4,
    &opcode_0xd5,
    &opcode_0xd6,
    &opcode_0xd7,
    &opcode_0xd8,
    &opcode_0xd9,
    &opcode_0xda,
    &opcode_0x0,
    &opcode_0xdc,
    &opcode_0x0,
    &opcode_0xde,
    &opcode_0xdf,
    &opcode_0xe0,
    &opcode_0xe1,
    &opcode_0xe2,
    &opcode_0x0,
    &opcode_0x0,
    &opcode_0xe5,
    &opcode_0xe6,
    &opcode_0xe7,
    &opcode_0xe8,
    &opcode_0xe9,
    &opcode_0xea,
    &opcode_0x0,
    &opcode_0x0,
    &opcode_0x0,
    &opcode_0xee,
    &opcode_0xef,
    &opcode_0xf0,
    &opcode_0xf1,
    &opcode_0xf2,
    &opcode_0xf3,
    &opcode_0x0,
    &opcode_0xf5,
    &opcode_0xf6,
    &opcode_0xf7,
    &opcode_0xf8,
    &opcode_0xf9,
    &opcode_0xfa,
    &opcode_0xfb,
    &opcode_0x0,
    &opcode_0x0,
    &opcode_0xfe,
    &opcode_0xff
};

const uint8_t instruction_ticks[] = {
    2, 6, 4, 4, 2, 2, 4, 4, 10, 4, 4, 4, 2, 2, 4, 4, // 0x0_
    2, 6, 4, 4, 2, 2, 4, 4,  4, 4, 4, 4, 2, 2, 4, 4, // 0x1_
    0, 6, 4, 4, 2, 2, 4, 2,  0, 4, 4, 4, 2, 2, 4, 2, // 0x2_
    4, 6, 4, 4, 6, 6, 6, 2,  0, 4, 4, 4, 2, 2, 4, 2, // 0x3_
    2, 2, 2, 2, 2, 2, 4, 2,  2, 2, 2, 2, 2, 2, 4, 2, // 0x4_
    2, 2, 2, 2, 2, 2, 4, 2,  2, 2, 2, 2, 2, 2, 4, 2, // 0x5_
    2, 2, 2, 2, 2, 2, 4, 2,  2, 2, 2, 2, 2, 2, 4, 2, // 0x6_
    4, 4, 4, 4, 4, 4, 2, 4,  2, 2, 2, 2, 2, 2, 4, 2, // 0x7_
    2, 2, 2, 2, 2, 2, 4, 2,  2, 2, 2, 2, 2, 2, 4, 2, // 0x8_
    2, 2, 2, 2, 2, 2, 4, 2,  2, 2, 2, 2, 2, 2, 4, 2, // 0x9_
    2, 2, 2, 2, 2, 2, 4, 2,  2, 2, 2, 2, 2, 2, 4, 2, // 0xa_
    2, 2, 2, 2, 2, 2, 4, 2,  2, 2, 2, 2, 2, 2, 4, 2, // 0xb_
    0, 6, 0, 6, 0, 8, 4, 8,  0, 2, 0, 0, 0, 6, 4, 8, // 0xc_
    0, 6, 0, 0, 0, 8, 4, 8,  0, 8, 0, 0, 0, 0, 4, 8, // 0xd_
    6, 6, 4, 0, 0, 8, 4, 8,  8, 2, 8, 0, 0, 0, 4, 8, // 0xe_
    6, 6, 4, 2, 0, 8, 4, 8,  6, 4, 8, 2, 0, 0, 4, 8  // 0xf_
};

const unsigned char extended_instruction_ticks[] = {
    8, 8, 8, 8, 8,  8, 16, 8,  8, 8, 8, 8, 8, 8, 16, 8, // 0x0_
    8, 8, 8, 8, 8,  8, 16, 8,  8, 8, 8, 8, 8, 8, 16, 8, // 0x1_
    8, 8, 8, 8, 8,  8, 16, 8,  8, 8, 8, 8, 8, 8, 16, 8, // 0x2_
    8, 8, 8, 8, 8,  8, 16, 8,  8, 8, 8, 8, 8, 8, 16, 8, // 0x3_
    8, 8, 8, 8, 8,  8, 12, 8,  8, 8, 8, 8, 8, 8, 12, 8, // 0x4_
    8, 8, 8, 8, 8,  8, 12, 8,  8, 8, 8, 8, 8, 8, 12, 8, // 0x5_
    8, 8, 8, 8, 8,  8, 12, 8,  8, 8, 8, 8, 8, 8, 12, 8, // 0x6_
    8, 8, 8, 8, 8,  8, 12, 8,  8, 8, 8, 8, 8, 8, 12, 8, // 0x7_
    8, 8, 8, 8, 8,  8, 12, 8,  8, 8, 8, 8, 8, 8, 12, 8, // 0x8_
    8, 8, 8, 8, 8,  8, 12, 8,  8, 8, 8, 8, 8, 8, 12, 8, // 0x9_
    8, 8, 8, 8, 8,  8, 12, 8,  8, 8, 8, 8, 8, 8, 12, 8, // 0xa_
    8, 8, 8, 8, 8,  8, 12, 8,  8, 8, 8, 8, 8, 8, 12, 8, // 0xb_
    8, 8, 8, 8, 8,  8, 12, 8,  8, 8, 8, 8, 8, 8, 12, 8, // 0xc_
    8, 8, 8, 8, 8,  8, 12, 8,  8, 8, 8, 8, 8, 8, 12, 8, // 0xd_
    8, 8, 8, 8, 8,  8, 12, 8,  8, 8, 8, 8, 8, 8, 12, 8, // 0xe_
    8, 8, 8, 8, 8,  8, 12, 8,  8, 8, 8, 8, 8, 8, 12, 8  // 0xf_
};

/*const uint8_t instruction_ticks[] = {
    4,   // NOP
    12,  // LD16
    8,   // LD8
    8,   // INC
    4,   // INC
    4,   // DEC
    8,   // LD8
    4,   // RLCA
    20,  // LD16
    8,   // ADD
    8,   // LD8
    8,   // DEC
    4,   // INC
    4,   // DEC
    8,   // LD8
    4,   // RRCA
    4,   // STOP
    12,  // LD16
    8,   // LD8
    8,   // INC
    4,   // INC
    4,   // DEC
    8,   // LD8
    4,   // RLA
    12,  // JR
    8,   // ADD
    8,   // LD8
    8,   // DEC
    4,   // INC
    4,   // DEC
    8,   // LD8
    4,   // RRA
    12,  // JR
    12,  // LD16
    8,   // LD8
    8,   // INC
    4,   // INC
    4,   // DEC
    8,   // LD8
    4,   // DAA
    12,  // JR
    8,   // ADD
    8,   // LD8
    8,   // DEC
    4,   // INC
    4,   // DEC
    8,   // LD8
    4,   // CPL
    12,  // JR
    12,  // LD16
    8,   // LD8
    8,   // INC
    12,  // INC
    12,  // DEC
    12,  // LD16
    4,   // SCF
    12,  // JR
    8,   // ADD
    8,   // LD8
    8,   // DEC
    4,   // INC
    4,   // DEC
    8,   // LD8
    4,   // CCF
    4,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    8,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    8,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    8,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    8,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    8,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    8,   // LD8
    4,   // LD8
    8,   // LD8
    8,   // LD8
    8,   // LD8
    8,   // LD8
    8,   // LD8
    8,   // LD8
    4,   // HALT
    8,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    4,   // LD8
    8,   // LD8
    4,   // LD8
    4,   // ADD
    4,   // ADD
    4,   // ADD
    4,   // ADD
    4,   // ADD
    4,   // ADD
    8,   // ADD
    4,   // ADD
    4,   // ADC
    4,   // ADC
    4,   // ADC
    4,   // ADC
    4,   // ADC
    4,   // ADC
    8,   // ADC
    4,   // ADC
    4,   // SUB
    4,   // SUB
    4,   // SUB
    4,   // SUB
    4,   // SUB
    4,   // SUB
    8,   // SUB
    4,   // SUB
    4,   // SBC
    4,   // SBC
    4,   // SBC
    4,   // SBC
    4,   // SBC
    4,   // SBC
    8,   // SBC
    4,   // SBC
    4,   // AND
    4,   // AND
    4,   // AND
    4,   // AND
    4,   // AND
    4,   // AND
    8,   // AND
    4,   // AND
    4,   // XOR
    4,   // XOR
    4,   // XOR
    4,   // XOR
    4,   // XOR
    4,   // XOR
    8,   // XOR
    4,   // XOR
    4,   // OR
    4,   // OR
    4,   // OR
    4,   // OR
    4,   // OR
    4,   // OR
    8,   // OR
    4,   // OR
    4,   // CP
    4,   // CP
    4,   // CP
    4,   // CP
    4,   // CP
    4,   // CP
    8,   // CP
    4,   // CP
    20,  // RET
    12,  // POP
    16,  // JP
    16,  // JP
    24,  // CALL
    16,  // PUSH
    8,   // ADD
    16,  // RST
    20,  // RET
    16,  // RET
    16,  // JP
    4,   // PREFIX
    24,  // CALL
    24,  // CALL
    8,   // ADC
    16,  // RST
    20,  // RET
    12,  // POP
    16   // JP
};
*/
