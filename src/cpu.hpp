//
//  cpu.hpp
//  GameBoy
//
//  Created by Robin Carlsson on 2016-06-01.
//  Copyright © 2016 Robin Carlsson. All rights reserved.
//

#pragma once

#include <cstdint>

#define CPU_INTERRUPT_VBLANK  0x1
#define CPU_INTERRUPT_LCDSTAT 0x2
#define CPU_INTERRUPT_TIMER   0x4
#define CPU_INTERRUPT_SERIAL  0x8
#define CPU_INTERRUPT_JOYPAD  0x10

struct Cartridge;
struct MMU;
struct Console;
struct GPU;

struct Interrupts
{
    bool master_enabled;
    uint8_t enabled;
    uint8_t flags;
};

struct CPU
{
    uint16_t SP;
    uint16_t PC;
    uint16_t AF;
    uint16_t BC;
    uint16_t DE;
    uint16_t HL;
    uint8_t* A;
    uint8_t* B;
    uint8_t* C;
    uint8_t* D;
    uint8_t* E;
    uint8_t* F;
    uint8_t* H;
    uint8_t* L;
    uint8_t temp;
    
    // Flags
    bool FZ;
    bool FN;
    bool FH;
    bool FC;
    
    uint64_t ticks;
    Interrupts interrupt;
};

void cpu_init(CPU*);
void cpu_init_defaults(CPU*, MMU*);
void cpu_step(Console*);
void cpu_update_flags(CPU*, bool, bool, bool, bool);
void cpu_update_flag_register(CPU*);
void cpu_interrupt_step(CPU*, GPU*, MMU*, Cartridge*);
void cpu_print_stack(CPU*, MMU*, Cartridge*);
void cpu_dump_memory(CPU*, MMU*, Cartridge*, uint16_t, uint16_t);

uint8_t*  get_first_operand8(CPU*, Cartridge*, MMU*);
uint8_t*  get_second_operand8(CPU*, Cartridge*, MMU*);
uint16_t* get_first_operand16(CPU*, Cartridge*, MMU*);
uint16_t* get_second_operand16(CPU*, Cartridge*, MMU*);

uint16_t get_operand_16(CPU*, MMU*, Cartridge*);
uint8_t  get_first_operand_8(CPU*, MMU*, Cartridge*);

uint8_t*  get_register_operand8(CPU*, uint8_t);
uint16_t* get_register_operand16(CPU*, uint8_t);
uint16_t* get_register_pointer(CPU*, uint8_t);

uint8_t rotl8 (uint8_t, unsigned int);
uint8_t rotr8 (uint8_t, unsigned int);

bool carry_rotl8(uint8_t);
bool carry_rotr8(uint8_t);

bool carry_arith_shiftl8(uint8_t);
bool carry_logic_shiftr8(uint8_t);

bool carry_add8(uint8_t, uint8_t);
bool carry_add16(uint16_t, uint16_t);
bool carry_sub8(uint8_t, uint8_t);
bool carry_sub16(uint16_t, uint16_t);
bool carry_inchi8(uint8_t);
bool carry_inchi16(uint16_t);
bool carry_addhi8(uint8_t, uint8_t);
bool carry_addhi16(uint16_t, uint16_t);
bool carry_subhi8(uint8_t, uint8_t);
bool carry_subhi16(uint16_t, uint16_t);
bool carry_dechi8(uint8_t);
bool carry_dechi16(uint16_t);

uint8_t  cpu_add8(CPU*, uint8_t, uint8_t);
uint16_t cpu_add16(CPU*, uint16_t, uint16_t);

uint8_t  cpu_addc8(CPU*, uint8_t, uint8_t);
uint16_t cpu_addc16(CPU*, uint16_t, uint16_t);

uint8_t  cpu_sub8(CPU*, uint8_t, uint8_t);
uint16_t cpu_sub16(CPU*, uint16_t, uint16_t);

uint8_t  cpu_subc8(CPU*, uint8_t, uint8_t);
uint16_t cpu_subc16(CPU*, uint16_t, uint16_t);

uint8_t  cpu_inc8(CPU*, uint8_t);
uint16_t cpu_inc16(CPU*, uint16_t);

uint8_t  cpu_dec8(CPU*, uint8_t);
uint16_t cpu_dec16(CPU*, uint16_t);

uint8_t  cpu_and8(CPU*, uint8_t, uint8_t);
uint16_t cpu_and16(CPU*, uint16_t, uint16_t);

uint8_t  cpu_xor8(CPU*, uint8_t, uint8_t);
uint16_t cpu_xor16(CPU*, uint16_t, uint16_t);

uint8_t  cpu_or8(CPU*, uint8_t, uint8_t);
uint16_t cpu_or16(CPU*, uint16_t, uint16_t);

void cpu_cp8(CPU*, uint8_t, uint8_t);
void cpu_cp16(CPU*, uint16_t, uint16_t);

void cpu_bit(CPU*, uint8_t, uint8_t);

uint8_t  stack_pop8(CPU*, MMU*, Cartridge*);
uint16_t stack_pop16(CPU*, MMU*, Cartridge*);

void stack_push8(CPU*, MMU*, Cartridge*, uint8_t);
void stack_push16(CPU*, MMU*, Cartridge*, uint16_t);




















