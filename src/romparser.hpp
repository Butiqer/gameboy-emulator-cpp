//
//  romparser.hpp
//  GameBoy
//
//  Created by Robin Carlsson on 2016-05-27.
//  Copyright © 2016 Robin Carlsson. All rights reserved.
//

#pragma once

#include <stddef.h>

struct Cartridge
{
    uint8_t* data;
    size_t   size;
    bool     loaded_successfully;
    
    Cartridge(const char*);
};