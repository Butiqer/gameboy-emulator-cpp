//
//  audio.hpp
//  GameBoy
//
//  Created by Robin Carlsson on 2016-12-27.
//  Copyright © 2016 Robin Carlsson. All rights reserved.
//

#pragma once

#include <SDL2/SDL.h>

void audio_init();
void audio_set_frequency(int);
void audio_set_frequency_c2(int);
void audio_set_frequency_c3(int);

struct Channel
{
    Sint16* buffer;
    int frequency;
    double wave;
};
