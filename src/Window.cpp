//
//  Renderer.cpp
//  GameBoy
//
//  Created by Robin Carlsson on 2016-10-03.
//  Copyright © 2016 Robin Carlsson. All rights reserved.
//

#include <iostream>

#include "Window.hpp"
#include "Console.hpp"
#include "mmu.hpp"
#include "audio.hpp"

void window_init(Window* window)
{
	SDL_SetMainReady();
    SDL_Init(SDL_INIT_EVERYTHING);
    window->width = 512;
    window->height = 512;
    window->window = SDL_CreateWindow("GameBoy", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, window->width, window->height, SDL_WINDOW_SHOWN);
    window->renderer = SDL_CreateRenderer(window->window, 0, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    window->tile_buffer = SDL_CreateTexture(window->renderer, SDL_PIXELFORMAT_RGB888, SDL_TEXTUREACCESS_STREAMING, TILE_BUFFER_WIDTH, TILE_BUFFER_HEIGHT);
}

void window_update(Window* window, Console* console)
{
    SDL_Event e;
    while(SDL_PollEvent(&e))
    {
        if(e.type == SDL_EventType::SDL_QUIT)
        {
            exit(0);
        }
        else if(e.type == SDL_EventType::SDL_KEYDOWN)
        {   
            if(e.key.keysym.scancode == SDL_SCANCODE_RETURN)
            {
                keys1 |= CONSOLE_KEYS_START;
            }
            else if(e.key.keysym.scancode == SDL_SCANCODE_Z)
            {
                keys1 |= CONSOLE_KEYS_A;
            }
            else if(e.key.keysym.scancode == SDL_SCANCODE_X)
            {
                keys1 |= CONSOLE_KEYS_B;
            }
            else if(e.key.keysym.scancode == SDL_SCANCODE_RIGHT)
            {
                keys2 |= CONSOLE_KEYS_RIGHT;
            }
            else if(e.key.keysym.scancode == SDL_SCANCODE_LEFT)
            {
                keys2 |= CONSOLE_KEYS_LEFT;
            }
            else if(e.key.keysym.scancode == SDL_SCANCODE_UP)
            {
                keys2 |= CONSOLE_KEYS_UP;
            }
            else if(e.key.keysym.scancode == SDL_SCANCODE_DOWN)
            {
                keys2 |= CONSOLE_KEYS_DOWN;
            }
        }
        else if(e.type == SDL_EventType::SDL_KEYUP)
        {
            if(e.key.keysym.scancode == SDL_SCANCODE_RETURN)
            {
                keys1 &= ~CONSOLE_KEYS_START;
            }
            else if(e.key.keysym.scancode == SDL_SCANCODE_Z)
            {
                keys1 &= ~CONSOLE_KEYS_A;
            }
            else if(e.key.keysym.scancode == SDL_SCANCODE_X)
            {
                keys1 &= ~CONSOLE_KEYS_B;
            }
            else if(e.key.keysym.scancode == SDL_SCANCODE_RIGHT)
            {
                keys2 &= ~CONSOLE_KEYS_RIGHT;
            }
            else if(e.key.keysym.scancode == SDL_SCANCODE_LEFT)
            {
                keys2 &= ~CONSOLE_KEYS_LEFT;
            }
            else if(e.key.keysym.scancode == SDL_SCANCODE_UP)
            {
                keys2 &= ~CONSOLE_KEYS_UP;
            }
            else if(e.key.keysym.scancode == SDL_SCANCODE_DOWN)
            {
                keys2 &= ~CONSOLE_KEYS_DOWN;
            }
        }
    }
}

void window_update_touch(Window* window, Console* console)
{
    static bool is_holding_down = false;
    static SDL_FingerID holding_down_finger_id = -1;
    SDL_Event e;
    while(SDL_PollEvent(&e))
    {
        keys1 = 0;
        keys2 = 0;
        if(e.type == SDL_EventType::SDL_FINGERDOWN)
        {
            if(e.tfinger.x > 0.4 && e.tfinger.x < 0.6)
            {
                keys1 |= CONSOLE_KEYS_START;
            }
            else if(e.tfinger.x > 0.8)
            {
                keys1 |= CONSOLE_KEYS_A;
            }
            else if(e.tfinger.x < 0.8 && e.tfinger.x > 0.6)
            {
                keys1 |= CONSOLE_KEYS_B;
            }
            else if(e.tfinger.x < 0.2 && e.tfinger.y < 0.9)
            {
                keys2 |= CONSOLE_KEYS_LEFT;
            }
            else if(e.tfinger.x > 0.2 && e.tfinger.x < 0.4 && e.tfinger.y < 0.9)
            {
                keys2 |= CONSOLE_KEYS_RIGHT;
            }
            else if(e.tfinger.x < 0.4 && e.tfinger.y > 0.9)
            {
                if(holding_down_finger_id == -1)
                {
                    holding_down_finger_id = e.tfinger.fingerId;
                    keys2 |= CONSOLE_KEYS_DOWN;
                }
            }
        }
        else if(e.type == SDL_EventType::SDL_FINGERUP)
        {
            if(e.tfinger.fingerId == holding_down_finger_id)
            {
                holding_down_finger_id = -1;
                keys2 &= ~CONSOLE_KEYS_DOWN;
            }
        }
    }
}

void window_draw(Window* window, Console* console)
{
    build_tile_buffer(window, console);
    
    SDL_RenderClear(window->renderer);
    //SDL_Rect dst {0, 0, 512, 600};
    //SDL_RenderCopy(window->renderer, window->tile_buffer, nullptr, &dst);
    draw_bg(window, console);
    draw_sprites(window, console);
    SDL_RenderPresent(window->renderer);
    SDL_RenderClear(window->renderer);
}

void window_set_fullscreen(Window* window, bool fullscreen)
{
    if(fullscreen)
        SDL_SetWindowFullscreen(window->window, SDL_WINDOW_FULLSCREEN);
    else
        SDL_SetWindowFullscreen(window->window, 0);
    SDL_GetWindowSize(window->window, &window->width, &window->height);
}

void update_audio(Console* console)
{
    {
        uint8_t lo = mmu_read_byte(&console->mmu, console->rom, 0xFF13);
        uint8_t hi = mmu_read_byte(&console->mmu, console->rom, 0xFF14);
        
        uint16_t freq = ((hi & 0x7) << 8) | lo;
        
        audio_set_frequency(131072 / (2048 - freq));
    }
    
    {
        uint8_t lo = mmu_read_byte(&console->mmu, console->rom, 0xFF18);
        uint8_t hi = mmu_read_byte(&console->mmu, console->rom, 0xFF19);
        
        uint16_t freq = ((hi & 0x7) << 8) | lo;
        
        audio_set_frequency_c2(131072 / (2048 - freq));
    }
    
    {
        uint8_t lo = mmu_read_byte(&console->mmu, console->rom, 0xFF1D);
        uint8_t hi = mmu_read_byte(&console->mmu, console->rom, 0xFF1E);
        
        uint16_t freq = ((hi & 0x7) << 8) | lo;
        
        audio_set_frequency_c3(131072 / (2048 - freq));
    }
}

void build_tile_buffer(Window* window, Console* console)
{
    uint32_t* buffer = new uint32_t[TILE_BUFFER_WIDTH * TILE_BUFFER_HEIGHT] {0};
    
    uint16_t vram_start = 0x8000;
    uint16_t vram_end   = 0x9800;
    uint16_t tile_x = 0;
    uint16_t tile_y = 0;
    uint16_t ypos = 0;
    
    for(uint16_t i = vram_start; i < vram_end; i += 2)
    {
        uint8_t byte1 = mmu_read_byte(&console->mmu, console->rom, i);
        uint8_t byte2 = mmu_read_byte(&console->mmu, console->rom, i + 1);
        
        for(int j = 0; j < 8; j++)
        {
            uint8_t bit1 = (byte1 >> (7 - j)) & 0x1;
            uint8_t bit2 = (byte2 >> (7 - j)) & 0x1;
            uint8_t color = ((bit1 << 1) | bit2) * (256 / 4);
            uint32_t argb_color = (uint32_t)color << 16 | (uint32_t)color << 8 | (uint32_t)color;
            buffer[(tile_y * 8 + ypos) * TILE_BUFFER_WIDTH + (tile_x * 8 + j)] = argb_color;
        }
        
        ypos += 1;
        if(ypos == 8)
        {
            ypos = 0;
            tile_x++;
            if(tile_x == TILE_BUFFER_WIDTH / 8)
            {
                tile_x = 0;
                tile_y++;
            }
        }
    }
    SDL_UpdateTexture(window->tile_buffer, nullptr, buffer, TILE_BUFFER_WIDTH * sizeof(uint32_t));
    delete [] buffer;
}

void draw_bg(Window* window, Console* console)
{
    uint16_t bgmap_start = 0x9800;
    uint16_t bgmap_end   = 0x9C00;
    int offset_x = (window->width - (20 * 16)) / 2;
    int offset_y = (window->height - (18 * 16)) / 2;
    
    for(int i = 0; i <= 32; i++)
    {
        for(int j = 0; j <= 32; j++)
        {
            uint16_t tile_id = mmu_read_byte(&console->mmu, console->rom, bgmap_start + (j * 32 + i));

            if(false) // För dr. mario
            {
                tile_id += 32 * 8;
                if(tile_id > 32 * 12)
                    tile_id -= 32 * 8;
            }
            
            uint8_t tile_x = tile_id % 32;
            uint8_t tile_y = tile_id / 32;
            SDL_Rect src {tile_x * 8, tile_y * 8, 8, 8};
            SDL_Rect dest {offset_x + i * 16, offset_y + j * 16, 16, 16};
            SDL_RenderCopy(window->renderer, window->tile_buffer, &src, &dest);
        }
    }
}

void draw_sprites(Window* window, Console* console)
{
    uint16_t oam_start = 0xFE00;
    uint16_t oam_end   = 0xFEA0;
    int offset_x = (window->width - (20 * 16)) / 2;
    int offset_y = (window->height - (18 * 16)) / 2;
    
    for(uint16_t i = oam_start; i < oam_end; i += 4)
    {
        uint8_t y       = mmu_read_byte(&console->mmu, console->rom, i);
        uint8_t x       = mmu_read_byte(&console->mmu, console->rom, i + 1);
        uint8_t tile_id = mmu_read_byte(&console->mmu, console->rom, i + 2);
        uint8_t flags   = mmu_read_byte(&console->mmu, console->rom, i + 3);
        
        uint8_t tile_x = tile_id % 32;
        uint8_t tile_y = tile_id / 32;
        //SDL_Rect src {tile_x * 8, tile_y * 8, 8, 8};
        SDL_Rect src {tile_x * 8, tile_y * 8, 8, 8};
        SDL_Rect dest {offset_x + (x - 8) * 2, offset_y + (y - 16) * 2, 16, 16};
        SDL_RenderCopy(window->renderer, window->tile_buffer, &src, &dest);
    }
}






















