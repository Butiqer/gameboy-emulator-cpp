//
//  main.cpp
//  GameBoy
//
//  Created by Robin Carlsson on 2016-05-27.
//  Copyright © 2016 Robin Carlsson. All rights reserved.
//


#include <ctime>
#include <iostream>

#include "console.hpp"
#include "Window.hpp"
#include "audio.hpp"

int main(int argc, const char * argv[])
{
    srand(time(nullptr));
    Window window;
    window_init(&window);
    
    audio_init();
    
    Cartridge rom {"GameboyResources/tetris.gb"};

	if (!rom.loaded_successfully) 
	{
		std::cerr << "Rom failed to load" << std::endl;
		return 1;
	}

    Console console {&rom};
    
    SDL_RenderClear(window.renderer);
    
    while(1)
    {
        cpu_step(&console);
        gpu_step(&console.gpu, &console.cpu, &console.mmu, console.rom);
        
        if(console.cpu.interrupt.master_enabled && (console.cpu.interrupt.enabled & console.cpu.interrupt.flags & CPU_INTERRUPT_VBLANK))
        {
            window_update(&window, &console);
            window_draw(&window, &console);
            update_audio(&console);
        }
        
        cpu_interrupt_step(&console.cpu, &console.gpu, &console.mmu, console.rom);
    }
    
    return 0;
}
