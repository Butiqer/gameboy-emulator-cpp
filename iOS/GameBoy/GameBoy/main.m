//
//  main.m
//  GameBoy
//
//  Created by Robin Carlsson on 2016-12-19.
//  Copyright © 2016 Robin Carlsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
