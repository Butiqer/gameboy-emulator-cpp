//
//  AppDelegate.h
//  GameBoy
//
//  Created by Robin Carlsson on 2016-12-19.
//  Copyright © 2016 Robin Carlsson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

