# GameBoy Emulator
Min egen [GameBoy](https://en.wikipedia.org/wiki/Game_Boy)-emulator skriven i c++.  
Alla instruktioner är inte implementerade, jag utvecklade den mot Tetris så just nu spelar den bara Tetris.  
[Windows build](https://www.dropbox.com/s/tecv7w8kfw6r9lm/GBEmulatorWIN.zip?dl=0)(Äldre build)

<img src="http://i.imgur.com/FlQN1cf.png" width="360"/>
<img src="https://i.imgur.com/pd4XsSs.png" width="360"/>

### Testad på
* Windows
* MacOS
* iOS

### Ej implementerat
* Alla 0xcb-instruktioner
* Timer
* DAA
* Alla ljudkanaler
* Scrolling
* Memory-banks

### Referenser
* http://www.devrs.com/gb/files/gbspec.txt
* [NO$GMB](http://problemkaputt.de/gmb.htm)
* http://pastraiser.com/cpu/gameboy/gameboy_opcodes.html
* http://gbdev.gg8.se/
* http://marc.rawer.de/Gameboy/Docs/GBCPUman.pdf

